# 动保商城移动web端

## 技术栈

- 依赖环境 Node.js 及 NPM （Yarn）安装器

- 项目框架

  项目主要基于 Vue + Vue-Router + Vuex + Webpack 框架搭建，使用 Vue CLI 3 脚手架。
  
  参考：

  Vue 官网 https://cn.vuejs.org

  Vue CLI 3 地址 https://cli.vuejs.org/zh/

  Vue-Router 地址 https://router.vuejs.org/zh/

  Vuex 地址 https://vuex.vuejs.org/zh/ 

- 其他

  axios 基于 promise 的 HTTP 库 https://www.npmjs.com/package/axios

  Vant 基于 Vue.js 的轻量的、可靠的移动端组件库 https://youzan.github.io/vant/#/zh-CN/

  vue-amap 基于 Vue.js 的高德地图组件库 https://elemefe.github.io/vue-amap/#/zh-cn/

## 项目依赖安装

  ```bash
  npm install # or: yarn
  ```

## 本地调代码运行

  ```bash
  npm run serve # or: yarn run serve
  ```

## Lints and fixes files

  ```bash
  npm run lint # or: yarn run lint
  ```

  代理地址配置可在 vue.config.js 里面配置

  ```js
  // ...
  /**
   * npm run serve 默认代理地址 
   */
  let target = 'https://t-api-mall.wens.com.cn'
  ```

## 代码构建及部署

- 构建代码

  ```bash
  npm run build # or: yarn run build 
  ```

- 代码部署

  执行完上面步骤之后，项目会生成一个 dist 文件目录，将该目录部署至生产环境即可。

## 项目大体结构

  ```bash
  .
  ├── README.md # 说明文档
  ├── babel.config.js # babel 构建配置文件
  ├── .eslintrc.js # eslint 配置文件
  ├── package-lock.json # 版本锁文件（npm 生成）
  ├── package.json # 依赖描述文件
  ├── postcss.config.js # 样式预编译配置
  ├── public # 前端静态资源目录（不编译）
  │   ├── css
  │   ├── favicon.ico
  │   ├── img
  │   ├── index.html
  │   ├── js
  │   ├── manifest.json
  │   └── robots.txt
  ├── src # 项目主要代码
  │   ├── App.vue # 主组件
  │   ├── api # api 对接后端接口存放文件
  │   ├── assets # 前端资源（）编译
  │   ├── components # 组件目录
  │   ├── main.js # 项目构建主入口
  │   ├── registerServiceWorker.js # pwa 缓存
  │   ├── router.js # 路由配置文件
  │   ├── store # 状态管理文件
  │   ├── utils # 一些公共js方法存放目录
  │   └── views # 页面视图文件，主要对应路由
  ├── vue.config.js # webpack 配置
  └── yarn.lock # 版本锁文件（yarn 生成）
  ```

## 页面布局设计标准

- 基于 iPhone 6，7，8
- 1px = 1/75 rem

