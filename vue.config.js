// let target = 'http://127.0.0.1:8080'
// let target = 'http://10.16.2.164:8081'
// let target = 'https://t-api-mall.wens.com.cn' // 开发环境
// let target = 'https://test-api-mall.wens.com.cn' // 测试环境
let target = 'http://10.11.50.11' // 测试环境
// let target = 'http://10.11.48.60:8888' // uat环境

module.exports = {
  // 基本路径
  publicPath: process.env.NODE_ENV === 'production' ? '/xp/' : '/',

  // 输出文件目录
  outputDir: 'dist',

  // 用于嵌套生成的静态资产（js，css，img，fonts）的目录。
  assetsDir: 'static',

  // 默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存。
  filenameHashing: true,

  // 以多页模式构建应用程序。
  pages: undefined,

  // eslint-loader 是否在保存的时候检查
  lintOnSave: true,

  // 是否使用包含运行时编译器的Vue核心的构建
  runtimeCompiler: false,

  // 默认情况下 babel-loader 会忽略所有 node_modules 中的文件。如果你想要通过 Babel 显式转译一个依赖，可以在这个选项中列出来。
  transpileDependencies: [],

  // 生产环境 sourceMap
  productionSourceMap: false,

  // cors 相关 https://jakearchibald.com/2017/es-modules-in-browsers/#always-cors
  // corsUseCredentials: false,
  // webpack 配置，键值对象时会合并配置，为方法时会改写配置
  // https://cli.vuejs.org/guide/webpack.html#simple-configuration
  // configureWebpack: (config) => {},

  // webpack 链接 API，用于生成和修改 webapck 配置
  // https://github.com/mozilla-neutrino/webpack-chain
  chainWebpack: config => {
    // 移除 prefetch 插件
    config.plugins.delete('prefetch')
  },

  // All options for webpack-dev-server are supported
  // https://webpack.js.org/configuration/dev-server/
  devServer: {
    open: true,
    host: '0.0.0.0',
    port: 8083,
    https: false,
    hotOnly: false,
    proxy: {
      '/mapi': {
        target, // 接口域名
        changeOrigin: true, // 是否跨域
        pathRewrite: {
          '^/mapi': '/ierp/'
        }
      }
    },
    overlay: {
      warnings: false,
      errors: false
    },
    before: app => {}
  },
  // 构建时开启多进程处理 babel 编译
  parallel: require('os').cpus().length > 1,

  // https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
  pwa: {
    // name: 'Wens Mall App',
    // themeColor: '#4DBA87',
    // msTileColor: '#000000',
    // appleMobileWebAppCapable: 'yes',
    // appleMobileWebAppStatusBarStyle: 'black',
    // iconPaths: {
    //   favicon32: 'img/icons/32x32.png',
    //   favicon16: 'img/icons/16x16.png',
    //   appleTouchIcon: 'img/icons/192x192.png',
    //   maskIcon: 'img/icons/192x192.png',
    //   msTileImage: 'img/icons/192x192.png'
    // },
    // workboxOptions: {
    //   skipWaiting: true,
    //   clientsClaim: true
    // }
  },

  // 第三方插件配置
  pluginOptions: {}
}
