import * as types from './mutationTypes'

export const setChannel = function ({ commit, state }, { data, flag }) {
  commit(types.SET_CHANNEL, { data, flag })
}

export const setRegion = function ({ commit, state }, { data, flag }) {
  commit(types.SET_REGION, { data, flag })
}

export const setCustomer = function ({ commit, state }, { data, flag }) {
  commit(types.SET_CUSTOMER, { data, flag })
}

export const setShoppingCartList = function ({ commit, getters }, data) {
  if (!data) return
  data = JSON.parse(JSON.stringify(data)) // 深拷贝

  commit(types.SET_SHOPPING_CART_LIST, data)
  commit(types.SET_SHOPPING_CART_COUNT, getters.shoppingCartCount(data))
}

export const setShoppingCartCount = function ({ commit }, count) {
  if (typeof count !== 'number') return
  commit(types.SET_SHOPPING_CART_COUNT, count)
}

export const setMessageList = function ({ commit, getters }, data) {
  if (!data) return
  data = JSON.parse(JSON.stringify(data)) // 深拷贝

  commit(types.SET_MESSAGE_LIST, data)
  commit(types.SET_MESSAGE_COUNT, getters.messageCount(data))
}

export const setMessageCount = function ({ commit }, count) {
  if (typeof count !== 'number') return
  commit(types.SET_MESSAGE_COUNT, count)
}

export const setIntoPage = function ({ commit }, count) {
  commit(types.SET_INTO_PAGE, count)
}

export const setCurrentRegion = function ({ commit }, data) {
  commit(types.SET_CURRENT_REGION, data)
}

export const setAllRegion = ({ commit }, data) => {
  commit(types.SET_ALL_REGION, data)
}

export const setAllChannel = ({ commit }, data) => {
  commit(types.SET_ALL_CHANNEL, data)
}

export const setDisplayMode = ({ commit }, data) => {
  commit(types.SET_DISPLAY_MODE, data)
}

export const setTokenInvalidFlag = ({ commit }, data) => {
  commit(types.SET_TOKEN_INVALID_FLAG, data)
}

export const setJustLoggedIn = ({ commit }, data) => {
  commit(types.SET_JUST_LOGGED_IN, data)
}
