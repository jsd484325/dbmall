export const SET_CHANNEL = 'SET_CHANNEL'
export const SET_REGION = 'SET_REGION'
export const SET_SHOPPING_CART_LIST = 'SET_SHOPPING_CART_LIST'
export const SET_SHOPPING_CART_COUNT = 'SET_SHOPPING_CART_COUNT'
export const SET_MESSAGE_LIST = 'SET_MESSAGE_LIST'
export const SET_MESSAGE_COUNT = 'SET_MESSAGE_COUNT'
export const SET_INTO_PAGE = 'SET_INTO_PAGE'
export const SET_CURRENT_REGION = 'SET_CURRENT_REGION'
export const SET_ALL_REGION = 'SET_ALL_REGION'
export const SET_ALL_CHANNEL = 'SET_ALL_CHANNEL'
export const SET_DISPLAY_MODE = 'SET_DISPLAY_MODE'
export const SET_CUSTOMER = 'SET_CUSTOMER'
export const SET_TOKEN_INVALID_FLAG = 'SET_TOKEN_INVALID_FLAG'
export const SET_JUST_LOGGED_IN = 'SET_JUST_LOGGED_IN'
