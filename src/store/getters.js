export const channel = state => state.channel
export const region = state => state.region
export const allRegion = state => state.allRegion
export const allChannel = state => state.allChannel
export const customer = state => state.customer
export const shoppingCartList = state => state.shoppingCartList
export const shoppingCartCount = state => data => {
  let count = 0
  Array.prototype.forEach.call(data, items => {
    // items.cartList.forEach(item => {
    //     count += item.qty
    // })

    count += items.cartList.filter(v => !(+v.entryType === 1)).length
  })
  return count
}
export const messageList = state => state.messageList
export const messageCount = state => data => {
  let count = 0
  Array.prototype.forEach.call(data, items => {
    count += items.returnData.length
  })
  return count
}
export const intoPage = state => state.intoPage
export const getDisplayMode = state => state.displayMode
export const justLoggedIn = state => state.justLoggedIn
