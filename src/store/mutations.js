import * as types from './mutationTypes'
import { setCookie, getCookie } from '@/utils/common'

const mutations = {
  [types.SET_CHANNEL] (state, { data, flag }) {
    if (flag === 'update') {
      state.channel.currentChannel = data
    } else {
      let arr = []

      arr = data.filter(v => {
        return v.billStatus === 'C'
      })

      state.channel.list.splice(0, state.channel.list.length, ...arr)

      let addCurrent = true

      if (getCookie('selectedChannel')) {
        let sc = JSON.parse(getCookie('selectedChannel'))

        state.channel.list.forEach(v => {
          if (v.channelId === sc.channelId) {
            state.channel.currentChannel = sc
            addCurrent = false
          }
        })
      }

      if (addCurrent) {
        state.channel.currentChannel = arr[0]
      }
    }

    if (state.channel.currentChannel) {
      setCookie('selectedChannel', JSON.stringify(state.channel.currentChannel), 7)
    } else {
      if (flag !== 'remainCookie') { // 保留cookie
        setCookie('selectedChannel', '', -1)
      }
    }
  },
  [types.SET_REGION] (state, { data, flag }) {
    if (flag === 'update') {
      state.region.current = data
    } else {
      state.region = data
    }

    if (state.region.current) {
      setCookie('selectedRegion', JSON.stringify(state.region.current), 7)
    }
  },
  [types.SET_CUSTOMER] (state, { data, flag }) {
    if (flag === 'update') {
      state.customer.current = data
    } else {
      state.customer.list.splice(0, state.customer.list.length, ...data)
      state.customer.current = data[0]

      // let addCurrent = true
      // if (sessionStorage.getItem('defaultCustomer')) {
      //   let dc = JSON.parse(sessionStorage.getItem('defaultCustomer'))

      //   state.customer.list.forEach(v => {
      //     if (v.customerId === dc.customerId) {
      //       state.customer.current = dc
      //       addCurrent = false
      //     }
      //   })
      // }
      // if (addCurrent) {
      //   state.customer.current = data[0]
      // }
    }

    if (state.customer.current) {
      sessionStorage.setItem('defaultCustomer', JSON.stringify(state.customer.current))
    } else {
      sessionStorage.setItem('defaultCustomer', JSON.stringify({}))
    }
  },
  [types.SET_SHOPPING_CART_LIST] (state, list) {
    state.shoppingCartList = list
  },
  [types.SET_SHOPPING_CART_COUNT] (state, count) {
    state.shoppingCartCount = count
  },
  [types.SET_MESSAGE_LIST] (state, list) {
    state.messageList = list
  },
  [types.SET_MESSAGE_COUNT] (state, count) {
    state.messageCount = count
  },
  [types.SET_INTO_PAGE] (state, data) {
    state.intoPage = data
  },
  [types.SET_CURRENT_REGION] (state, data) {
    state.currentRegion = data
  },
  [types.SET_ALL_REGION] (state, data) {
    state.allRegion = data
  },
  [types.SET_ALL_CHANNEL] (state, data) {
    state.allChannel = data
  },
  [types.SET_DISPLAY_MODE] (state, data) {
    state.displayMode = data
  },
  [types.SET_TOKEN_INVALID_FLAG] (state, data) {
    state.tokenInvalidFlag = data
  },
  [types.SET_JUST_LOGGED_IN] (state, data) {
    state.justLoggedIn = data
  }
}

export default mutations
