const state = {
  // 地区
  region: [],
  // 本地
  currentRegion: {},
  // 所有地区
  allRegion: [],
  // 渠道/销售部
  channel: {
    currentChannel: {},
    list: []
  },
  // 所有渠道/销售部
  allChannel: [],
  // 客户
  customer: {
    current: {},
    list: []
  },
  // 购物车列表
  shoppingCartList: [],
  // 购物车数量
  shoppingCartCount: 0,
  // 消息列表
  messageList: [],
  // 消息数量
  messageCount: 0,
  // 0：刷新或打开新页面，1:页面跳转
  intoPage: 0,
  // 商品列表显示方式: 0 - 横列显示, 1 - 竖列显示
  displayMode: 0,
  // 记录token失效是否需要跳转flag
  tokenInvalidFlag: true,
  // 记录是否刚登录状态
  justLoggedIn: false
}

export default state
