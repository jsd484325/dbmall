import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { getCookie, getUrlQueryString } from './utils/common'
import { api } from './api'
import 'amfe-flexible'
import {
  Toast,
  Lazyload,
  Button,
  Cell,
  CellGroup,
  Icon,
  Col,
  Row,
  Popup,
  Calendar,
  Checkbox,
  CheckboxGroup,
  DatetimePicker,
  Field,
  NumberKeyboard,
  Picker,
  RadioGroup,
  Radio,
  Rate,
  Search,
  Slider,
  Stepper,
  Uploader,
  ActionSheet,
  Dialog,
  Loading,
  Notify,
  Overlay,
  PullRefresh,
  Circle,
  Collapse,
  CollapseItem,
  CountDown,
  Divider,
  List,
  NoticeBar,
  Progress,
  Step,
  Steps,
  Swipe,
  SwipeItem,
  Tag,
  IndexBar,
  IndexAnchor,
  NavBar,
  Pagination,
  Sidebar,
  SidebarItem,
  Tab,
  Tabs,
  Tabbar,
  TabbarItem,
  GoodsAction,
  GoodsActionIcon,
  GoodsActionButton
} from 'vant'
// import 'vant/lib/index.css' // 通过 babel-plugin-import 按需加载vant样式
import './assets/css/reset.less'
import './assets/font/font.less'
import './assets/iconfont/iconfont.js' // 引入iconfont
import 'video.js/dist/video-js.css'
import initAmap from './utils/initAmap'
import BigDecimal from 'js-big-decimal'
// import bonreeAPM from '../public/js/agent.vue.min.js'
import Vconsole from 'vconsole'

// 全局注册svg-icon组件
import SvgIcon from '@/components/svg-icon/index'
Vue.component('svg-icon', SvgIcon)

/**
 * 博睿APM——初始化探针
 */
// bonreeAPM.config({
//   userHttps: true
// })

/**
 * vConsole调试工具
 * 生产环境才应用
 */
if (process.env.NODE_ENV === 'production') {
  const vConsole = new Vconsole()
  Vue.use(vConsole)
}

// Vue.use(Vant)
// Lazyload图片懒加载
Vue.use(Lazyload, {
  error: require('@/assets/images/default2.png'),
  attempt: 1
}).use(Button).use(Cell).use(CellGroup).use(Icon).use(Col).use(Row).use(Popup).use(Calendar).use(Checkbox).use(CheckboxGroup).use(DatetimePicker).use(Field).use(NumberKeyboard)
  .use(Picker).use(RadioGroup).use(Radio).use(Rate).use(Search).use(Slider).use(Stepper).use(Uploader).use(ActionSheet).use(Dialog).use(Loading).use(Notify).use(Overlay).use(PullRefresh)
  .use(Circle).use(Collapse).use(CollapseItem).use(CountDown).use(Divider).use(List).use(NoticeBar).use(Progress).use(Step).use(Steps).use(Swipe).use(SwipeItem).use(Tag).use(IndexBar)
  .use(IndexAnchor).use(NavBar).use(Pagination).use(Sidebar).use(SidebarItem).use(Tab).use(Tabs).use(Tabbar).use(TabbarItem).use(GoodsAction).use(GoodsActionIcon).use(GoodsActionButton)
// 全局配置Toast
Toast.allowMultiple() // 允许同时存在多个Toast
Toast.setDefaultOptions('loading', {
  forbidClick: true // 禁用背景点击
})
Toast.setDefaultOptions('fail', {
  duration: 4000 // 展示时长(ms)
})

// 初始化AMap
initAmap(Vue)

// 全局挂载js-big-decimal
Vue.prototype.$BigDecimal = BigDecimal

// 阻止启动时生成生产提示
Vue.config.productionTip = false

// 全局过滤器-四舍五入保留两位小数
Vue.filter('keepTwoNum', function (val) {
  if (isNaN(val)) return
  val = Number(val)
  return val.toFixed(2)
})

// 全局过滤器-四舍五入保留0位小数
Vue.filter('keepZeroNum', function (val) {
  if (isNaN(val)) return
  val = Number(val)
  return val.toFixed(0)
})

// 全局过滤器-不足10补零
Vue.filter('fillZero', function (val) {
  if (isNaN(val)) return
  val = Number(val)
  return val < 10 ? '0' + val : '' + val
})

// 注册一个全局自定义指令 `v-focus`,自动聚焦
Vue.directive('focus', {
  inserted (el) {
    // 聚焦元素
    el.focus()
  }
})

// 路由拦截，判断是否保存登录信息
router.beforeEach((to, from, next) => {
  store.dispatch('setIntoPage', from.name === null ? 0 : 1) // 保存当前页面更改状态（0为刷新或新页面，1为页面跳转）

  if (store.state.intoPage === 0 && sessionStorage.getItem('token')) {
    store.dispatch('setJustLoggedIn', true) // 设置刚登录状态
  }

  var userToken = getCookie('user_token')
  let newToken = getUrlQueryString(location.href, 'token') // 是否带token自动登录
  if (userToken && !newToken && location.href.indexOf('/jumpPage') < 0) {
    if (!sessionStorage.getItem('token') && store.state.intoPage === 0) {
      store.dispatch('setJustLoggedIn', true) // 设置刚登录状态
      sessionStorage.setItem('token', userToken)

      api.userInfo().then(res => { // 查询用户信息（包括是否为司机）
        if (res.success) {
          sessionStorage.setItem('br-custom-key', res.data.phone)
          sessionStorage.setItem('userInfo', JSON.stringify(res.data))
          sessionStorage.setItem('isDriver', res.data.isDriver)
          // 判断是否为司机
          if (res.data.isDriver === '1') { // 如果是司机
            // 查询当前用户的所有客户
            api.commonCall({}, 'getCustomerList').then(res => {
              if (res.success) {
                // 保存客户列表到vuex
                if (res.data && res.data.list && res.data.list.length) {
                  store.dispatch('setCustomer', { data: res.data.list })
                } else {
                  store.dispatch('setCustomer', { data: [] })
                }
              } else {
                store.dispatch('setCustomer', { data: [] })
              }
            })
            next({ path: '/order', replace: true, query: { orderType: 0, index: 0, businessIndex: 0, biztypeId: '' } })
          } else { // 如果不是司机
            // 查询当前用户的客户列表及默认客户（列表第一个）
            api.commonCall({}, 'getCustomerList').then(res => {
              if (res.success) {
                if (res.data && res.data.list && res.data.list.length) {
                  store.dispatch('setCustomer', { data: res.data.list })
                } else {
                  store.dispatch('setCustomer', { data: [] })
                }
              } else {
                store.dispatch('setCustomer', { data: [] })
              }

              if (getCookie('selectedRegion')) {
                api.customerAuthQuery({ customerId: store.state.customer && store.state.customer.current && store.state.customer.current.customerId }).then(res => {
                  if (res.success && res.status !== 'noData') {
                    store.dispatch('setAllChannel', res.data.list.slice(0, res.data.list.length)) // 保存所有渠道/销售部信息

                    store.dispatch('setChannel', { data: res.data.list })

                    var validRegion = store.getters.region.data ? store.getters.region.data.filter(item => {
                      let obj
                      store.getters.channel.list.forEach(i => {
                        if (item.name.indexOf(i.channelAreaName.split('_')[0]) > -1) {
                          obj = item
                        }
                      })
                      return obj
                    }) : []

                    var current = JSON.parse(getCookie('selectedRegion'))
                    current = validRegion.find(v => current.number === v.number) ? current : validRegion[0]
                    // 根据渠道筛选地区
                    store.dispatch('setRegion', {
                      data: {
                        current,
                        data: validRegion
                      }
                    })

                    // let areaName = store.getters.region.current && store.getters.region.current.name
                    // let list = store.getters.channel.list.filter(v => {
                    //   return areaName.indexOf(v.channelAreaName.split('_')[0]) > -1
                    // })
                    // store.dispatch('setChannel', { data: list.length ? list : store.getters.channel.list })
                  } else {
                    store.dispatch('setChannel', { data: [] })
                  }

                  if (to.path === '/loginRandom' || to.path === '/login') {
                    next({ path: '/' })
                  } else {
                    next()
                  }
                })
              } else {
                if (to.path === '/loginRandom' || to.path === '/login') {
                  next({ path: '/' })
                } else {
                  next()
                }
              }
            })
          }
        } else {
          // 登出
          api.logout().then(res => {
            sessionStorage.removeItem('token')
            sessionStorage.removeItem('br-custom-key')
            sessionStorage.removeItem('defaultCustomer')
            sessionStorage.removeItem('isDriver')
            sessionStorage.removeItem('groupBuyingCache')
            sessionStorage.removeItem('flashSaleCache')
            next({ path: '/loginRandom' })
          })
        }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
