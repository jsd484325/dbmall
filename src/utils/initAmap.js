/**
 * 引入vue-amap
 * 20190801 by Fanglin Zhang
 */
import VueAMap from 'vue-amap'
// import { lazyAMapApiLoaderInstance } from 'vue-amap'

const initAmap = Vue => {
  setTimeout(() => {
    localStorage.removeItem('_AMap_raster')
    Vue.use(VueAMap)
  }, 0)
  // 初始化vue-amap
  VueAMap.initAMapApiLoader({
    // 高德的key
    key: '7920e637e36cec1bfba6b33bddc4a998',
    // 插件集合
    plugin: [
      'AMap.Scale',
      'AMap.ToolBar',
      'AMap.Geolocation',
      'AMap.Geocoder'
    ],
    uiVersion: '1.0.11', // 版本号
    v: '1.4.4'
  })

  // lazyAMapApiLoaderInstance.load().then(() => {
  //   // your code ...
  //   this.map = new AMap.Map('amapContainer', {
  //     center: new AMap.LngLat(121.59996, 31.197646)
  //   })
  // })
}

export default initAmap
