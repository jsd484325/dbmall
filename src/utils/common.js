/* eslint-disable */
import { Bitmap } from '@/utils/bitmap'

/**
 * 倒计时
 * @param systemDate 开始时间
 * @param endDate 目标时间
 * @param callback 回调函数
 */
export function countdown (systemDate, endDate, callback) {
  // 目标时间
  let tarTime = new Date(endDate * 1)
  // 当前时间（标准格式）
  let newTime = new Date(systemDate * 1)
  // 目标时间距离1970年之间的毫秒差
  let tarSpan = tarTime.getTime()
  // 当前时间距离1970年之间的毫秒差
  let newSpan = newTime.getTime()

  // 当前时间距离目标时间的毫秒差
  let diffTime = tarSpan - newSpan
  // 当前时间大于目标时间直接return
  if (isNaN(diffTime) || diffTime <= 0) {
    callback({
      hour: '00',
      minute: '00',
      second: '00',
      time: '00:00:00'
    })
  } else {
    // 包含多少个小时（1000毫秒等于1秒，60秒等于一分钟，60分钟等于1小时）
    let hour = Math.floor(diffTime / (1000 * 60 * 60))
    // 算出当前得出的小时等于多少毫秒
    let hourMs = hour * 60 * 60 * 1000
    // 剩下的毫秒数
    let spanMs = diffTime - hourMs
    // 求剩下多少分钟
    let minute = Math.floor(spanMs / (1000 * 60))
    // 算出当前得出的分钟等于多少毫秒
    let minuteMs = minute * 60 * 1000
    // 得出剩下多少毫秒数
    let spanMs2 = spanMs - minuteMs
    // 求剩下多少秒
    let second = Math.floor(spanMs2 / 1000)
    // 倒计时为0直接return
    // if (hour <= 0 && minute <= 0 && second <= 0) {
    //     callback(zero(hour) + ":" + zero(minute) + ":" + zero(second));
    //     return;
    // }
    callback({
      hour: zero(hour),
      minute: zero(minute),
      second: zero(second),
      time: zero(hour) + ':' + zero(minute) + ':' + zero(second)
    })
    setTimeout(function () {
      countdown(newSpan += 1000, endDate, callback)
    }, 1000)
  }
}

/**
 * 判断一个对象是否是数组，参数不是对象或者不是数组，返回false
 *
 * @param {Object} arg 需要测试是否为数组的对象
 * @return {Boolean} 传入参数是数组返回true，否则返回false
 */
export function isArray (arg) {
  if (typeof arg === 'object') {
    return Object.prototype.toString.call(arg) === '[object Array]'
  }
  return false
}

/**
 * 判断一个对象是否是对象，参数不是对象或者不是数组，返回false
 *
 * @param {Object} arg 需要测试是否为对象
 * @return {Boolean} 传入参数是对象返回true，否则返回false
 */
export function isObject (arg) {
  if (typeof arg === 'object') {
    return Object.prototype.toString.call(arg) === '[object Object]'
  }
  return false
}

/**
 * 补零函数
 * @param value
 */
export function zero (value) {
  return value < 10 ? '0' + value : value
}

/**
 * 获取滚动条距离顶端的距离
 * @return {}支持IE6
 */
export function getScrollTop () {
  let scrollPos
  if (window.pageYOffset) {
    scrollPos = window.pageYOffset
  } else if (document.compatMode && document.compatMode !== 'BackCompat') {
    scrollPos = document.documentElement.scrollTop
  } else if (document.body) {
    scrollPos = document.body.scrollTop
  }
  return scrollPos
}

/**
 * 获取窗口可视范围的高度
 */
export function getClientHeight () {
  let clientHeight = 0
  if (document.body.clientHeight && document.documentElement.clientHeight) {
    clientHeight = document.body.clientHeight < document.documentElement.clientHeight ? document.body.clientHeight : document.documentElement.clientHeight
  } else {
    clientHeight = document.body.clientHeight > document.documentElement.clientHeight ? document.body.clientHeight : document.documentElement.clientHeight
  }
  return clientHeight
}

/**
 * 获取文档内容实际高度
 */
export function getScrollHeight () {
  return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
}

/**
 * scrollTop animation
 * @param el dom 对象
 * @param from 当前scrollTop值
 * @param to 要去到scrollTo的值
 * @param duration 执行动画时间
 * @param endCallback 回调函数
 */
export function scrollTop (el, from = 0, to, duration = 500, endCallback) {
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = (
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function (callback) {
        return window.setTimeout(callback, 1000 / 60)
      }
    )
  }
  const difference = Math.abs(from - to)
  const step = Math.ceil(difference / duration * 50)

  function scroll (start, end, step) {
    if (start === end) {
      endCallback && endCallback()
      return
    }

    let d = (start + step > end) ? end : start + step
    if (start > end) {
      d = (start - step < end) ? end : start - step
    }

    if (el === window) {
      window.scrollTo(d, d)
    } else {
      el.scrollTop = d
    }
    window.requestAnimationFrame(() => scroll(d, end, step))
  }

  scroll(from, to, step)
}

/**
 * scrollLeft animation
 * @param el dom 对象
 * @param from 当前scrollLeft值
 * @param to 要去到scrollLeft的值
 * @param duration 执行动画时间
 * @param endCallback 回调函数
 */
export function scrollLeft (el, from = 0, to, duration = 500, endCallback) {
  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = (
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function (callback) {
        return window.setTimeout(callback, 1000 / 60)
      }
    )
  }
  const difference = Math.abs(from - to)
  const step = Math.ceil(difference / duration * 50)

  function scroll (start, end, step) {
    if (start === end) {
      endCallback && endCallback()
      return
    }

    let d = (start + step > end) ? end : start + step
    if (start > end) {
      d = (start - step < end) ? end : start - step
    }

    el.scrollLeft = d
    window.requestAnimationFrame(() => scroll(d, end, step))
  }

  scroll(from, to, step)
}

/*
 * 生成uuid
 */
export function uuid (len, radix) {
  var chars, i, r, uuid
  chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('')
  uuid = []
  i = void 0
  radix = radix || chars.length
  if (len) {
    i = 0
    while (i < len) {
      uuid[i] = chars[0 | Math.random() * radix]
      i++
    }
  } else {
    r = void 0
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
    uuid[14] = '4'
    i = 0
    while (i < 36) {
      if (!uuid[i]) {
        r = 0 | Math.random() * 16
        uuid[i] = chars[i === 19 ? r & 0x3 | 0x8 : r]
      }
      i++
    }
  }
  return uuid.join('')
}

/**
 * 验证手机号码是否正确
 * 1开头，根据市场手机号码第二位则则有【3,4,5,6,7,8,9】，第三位之后则是数字【0-9】
 * 返回true则手机号码验证通过
 */
export function isPhone (phonenum) {
  let reg = /^(13|14|15|16|17|18|19)[0-9]{9}$/

  if (reg.test(phonenum)) {
    return true
  }

  // let reg1 = /^[+](\\d{1,3})?\\-s?(13|14|15|16|17|18|19)[0-9]{9}$/
  // let reg2 = /^(13|14|15|16|17|18|19)[0-9]{9}$/
  // let reg3 = /^0[1,2]{1}\\d{1}-?\\d{8}$/
  // let reg4 = /^0[3-9] {1}\\d{2}-?\\d{7,8}$/
  // let reg5 = /^0[1,2]{1}\\d{1}-?\\d{8}-(\\d{1,4})$/
  // let reg6 = /^0[3-9]{1}\\d{2}-? \\d{7,8}-(\\d{1,4})$/

  // if (reg1.test(phonenum) || reg2.test(phonenum) || reg3.test(phonenum) || reg4.test(phonenum) || reg5.test(phonenum) || reg6.test(phonenum)) {
  //   return true
  // }

  return false
}

/*
 * 判断是否有中文
 */
export function isChinese (character) {
  let reg = /[\u4e00-\u9fa5]/
  if (reg.test(character)) {
    return true
  }
  return false
}

/**
 * 验证密码是否正确
 * 由字母加数字或符号（至少两种）组成，6-20位半角字符，区分大小写
 * 返回true则手机号码验证通过
 */
export function isPassword (password) {
  // 由字母加数字或符号组成的6-20位半角字符，区分大小写
  let reg = /([\x21-\x2f\x3a-\x40\x5b-\x60\x7B-\x7F]|[a-zA-Z]|[0-9]){6,}$/
  // 半角符号
  let reg1 = /([\x21-\x2f\x3a-\x40\x5b-\x60\x7B-\x7F])+/
  // 字母
  let reg2 = /([a-zA-Z])+/
  // 数字
  let reg3 = /([0-9])+/
  // 使用字符种数
  let count = 0
  if (!reg.test(password)) {
    return false
  }
  if (reg1.test(password)) {
    count++
  }
  if (reg2.test(password)) {
    count++
  }
  if (reg3.test(password)) {
    count++
  }
  if (count < 2) {
    return false
  } else {
    return true
  }
}

/**
 * 设置cookie
 * name为cookie的名字，value是值，expiredays为过期时间（天数）
 */
export function setCookie (name, value, expiredays) {
  let exdate = new Date()
  exdate.setDate(exdate.getDate() + expiredays)
  document.cookie = name + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toGMTString())
}

/**
 * 读取cookie
 * 需要注意的是cookie是不能存中文的，如果需要存中文，解决方法是后端先进行编码encode()，前端取出来之后用decodeURI('string')解码。（安卓可以取中文cookie，IOS不行）
 */
export function getCookie (c_name) {
  if (document.cookie.length > 0) {
    let c_start = document.cookie.indexOf(c_name + '=')
    if (c_start != -1) {
      c_start = c_start + c_name.length + 1
      let c_end = document.cookie.indexOf(';', c_start)
      if (c_end == -1) {
        c_end = document.cookie.length
      }
      return unescape(document.cookie.substring(c_start, c_end))
    } else {
      return false
    }
  } else {
    return false
  }
}

// 时间戳转时间：如果没有传入时间戳就返回获取当前时间（2018 ? 01 ?10 ?）
export function timestampToYMD (timestamp, yearConnector, monthConnector, dateConnector) {
  timestamp = timestamp.toString()
  let time
  if (timestamp) {
    if (timestamp.length === 10) {
      timestamp = parseInt(timestamp) * 1000
    } else {
      timestamp = parseInt(timestamp)
    }
    time = new Date(timestamp)
  } else {
    time = new Date()
  }
  // 获取年
  let fullYear = time.getFullYear()
  // 获取月(0 ~ 11),所以要加1
  let month = time.getMonth() + 1
  // 获取日
  let date = time.getDate()
  return fullYear + yearConnector + zero(month) + monthConnector + zero(date) + dateConnector
}

// 时间戳转时间：如果没有传入时间戳就返回获取当前时间（10:01:01）
export function timestampToHMS (timestamp) {
  // 字符串才能获取长度
  timestamp = timestamp.toString()
  let time
  if (timestamp) {
    if (timestamp.length === 10) {
      timestamp = parseInt(timestamp) * 1000
    } else {
      timestamp = parseInt(timestamp)
    }
    time = new Date(timestamp)
  } else {
    time = new Date()
  }
  // 获取时
  let hours = time.getHours()
  // 获取分
  let minutes = time.getMinutes()
  // 获取秒
  let seconds = time.getSeconds()
  return zero(hours) + ':' + zero(minutes) + ':' + zero(seconds)
}

// 正则表达式判断身份证格式
export function checkIDCard (idcard) {
  // 15位数身份证验证正则表达式：
  let isIDCard1 = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/

  // 18位数身份证验证正则表达式 ：
  let isIDCard2 = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/
  if (isIDCard1.test(idcard) || isIDCard2.test(idcard)) {
    return true
  } else {
    return false
  }
}

// 正则表达式判断统一社会信用代码格式
export function checkUSCC (uscc) {
  // 数字和大写字母验证，15或18位
  let isUscc = /(^[0-9A-Z]{15}$)|(^[0-9A-Z]{18}$)/
  if (isUscc.test(uscc)) {
    return true
  } else {
    return false
  }
}

// 正则表达式判断邮箱格式
export function checkEmail (email) {
  // let isEmail=/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
  let isEmail = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/

  if (isEmail.test(email)) {
    return true
  } else {
    return false
  }
}

// 正则表达式判断银行卡格式
/**
 * 一般贷记卡的卡号位数都是16位，借记卡位数根据银行不同，是16位到19位不等
 * 当前方法只校验16位或17位或18位或19位
 */
export function checkBankCard (bankcard) {
  let isBankcard = /^([1-9]{1})(\d{15}|\d{16}|\d{17}|\d{18})$/
  if (isBankcard.test(bankcard)) {
    return true
  } else {
    return false
  }
}

// 正则表达式判断车牌号格式
/**
* 第一：普通汽车
* 车牌号格式：汉字 + A-Z + 5位A-Z或0-9（车牌号不存在字母I和O防止和1、0混淆）
* （只包括了普通车牌号，教练车，警等车牌号 。部分部队车，新能源不包括在内）
* 京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼军空海北沈兰济南广成使领
* 普通汽车规则："[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}"
*
*  第二：新能源车
* 组成：省份简称（1位汉字）+发牌机关代号（1位字母）+序号（6位），总计8个字符，序号不能出现字母I和字母O
* 通用规则：不区分大小写，第一位：省份简称（1位汉字），第二位：发牌机关代号（1位字母）
* 序号位：
* 小型车，第一位：只能用字母D或字母F，第二位：字母或者数字，后四位：必须使用数字
* ---([DF][A-HJ-NP-Z0-9][0-9]{4})
* 大型车，前五位：必须使用数字，第六位：只能用字母D或字母F。
* ----([0-9]{5}[DF])
* 新能源车规则："[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF][A-HJ-NP-Z0-9][0-9]{4}))"
*
* 总规则："([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1})"
*/
export function checkCarNumber (carNum) {
  // 传统车牌号验证正则表达式
  let isCarNum1 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳使领]$/
  // 新能源车牌号验证正则表达式
  let isCarNum2 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z](([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4}))$/

  if (isCarNum1.test(carNum) || isCarNum2.test(carNum)) {
    return true
  } else {
    return false
  }
}

// 正则表达式判断固定电话
export function fixedTelphone (fixedtelphone) {
  // let isfixedtelphone=/(^[0-9]{3,4}-[0-9]{3,8}$)|(^[0-9]{3,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}1[0-9]{10}$)/;
  let isfixedtelphone = /^\d{3}-\d{8}|\d{4}-\d{7}$/

  if (isfixedtelphone.test(fixedtelphone)) {
    return true
  } else {
    return false
  }
}

/**
 * 获取绝对位置的纵坐标
 * @param element
 * @returns {exports.default.props.offsetTop|{type, default}|default.props.offsetTop|number}
 */
export function getElementOffsetTop (element) {
  let actualTop = element.offsetTop
  let current = element.offsetParent
  while (current !== null) {
    actualTop += current.offsetTop
    current = current.offsetParent
  }
  return actualTop
}

/**
 * 获取绝对位置的横坐标
 * @param element
 * @returns {number}
 */
export function getElementOffsetLeft (element) {
  let actualLeft = element.offsetLeft
  let current = element.offsetParent
  while (current !== null) {
    actualLeft += current.offsetLeft
    current = current.offsetParent
  }
  return actualLeft
}

/**
 * js函数节流
 * @param method 要执行的函数及执行环境
 * @param delay 延迟时间，单位毫秒
 * @returns {Function}
 */
export function _throttle (method, delay = 50) {
  // 首先在函数内定义一个变量timer，然后返回一个函数，由于在返回函数中包含对timer的引用，因此timer并不会立即被清除。
  let timer = null
  return function () {
    let context = this
    let args = arguments
    clearTimeout(timer)
    timer = setTimeout(function () {
      method.apply(context, args)
    }, delay)
  }
}

/**
 * js函数防抖
 * @param method 要执行的函数及执行环境
 * @param delay 延迟时间，单位毫秒
 * @returns {Function}
 */
export function _debounce (method, delay = 200) {
  let timer = null
  return function () {
    let context = this
    let args = arguments
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(function () {
      timer = null
      method.apply(context, args)
    }, delay)
  };
}

/**
 * 验证后缀名是图片格式
 * @param str
 * @returns {boolean}
 */
export function checkSuffixIsImg (str) {
  let strRegex = '(.jpg|.png|.gif|.jpeg)$'
  let re = new RegExp(strRegex)
  if (re.test(str.toLowerCase())) {
    return true
  } else {
    return false
  }
}

/**
 * 对数组进行分组
 * @param data 要分组的数据
 * @param key 分组key值
 * @returns {Array} 返回新的分组数组
 */
export const formatDataByGroup = (data, key) => {
  let result = []
  let keyList = []
  data.map((item) => {
    if (keyList.indexOf(item[key]) > -1) {
      result.map((resultItem) => {
        if (resultItem.key === item[key]) {
          resultItem.value.push(item)
        }
      })
    } else {
      keyList.push(item[key])
      let temp = []
      temp.push(item)
      result.push({ 'key': item[key], 'value': temp, 'open': false })
    }
  })
  return result
}

/**
 * 数组按照个数进行分组
 * @param arr 要分组的数组
 * @param num 要分组的个数
 * @returns {Array} 返回新的分组数组
 */
export const arrayGroupingByQuantity = (arr, num) => {
  let result = []
  for (let i = 0, len = arr.length; i < len; i += num) {
    result.push(arr.slice(i, i + num))
  }
  return result
}

/**
 * 获取改变可见属性的事件的名称
 * @returns { visibilityChange, changeState } 返回可见属性事件和状态名称
 */
export const getVisibilityChange = () => {
  // 设置改变可见属性的事件的名称
  let changeState, visibilityChange

  if (typeof document.hidden !== 'undefined') {
    visibilityChange = 'visibilitychange'
    changeState = 'visibilityState'
  } else if (typeof document.mozHidden !== 'undefined') {
    visibilityChange = 'mozvisibilitychange'
    changeState = 'mozVisibilityState'
  } else if (typeof document.msHidden !== 'undefined') {
    visibilityChange = 'msvisibilitychange'
    changeState = 'msVisibilityState'
  } else if (typeof document.webkitHidden !== 'undefined') {
    visibilityChange = 'webkitvisibilitychange'
    changeState = 'webkitVisibilityState'
  } else {
    visibilityChange = null
    changeState = null
  }

  return { visibilityChange, changeState }
}

/**
 * 获取浏览器地址参数
 */
export const getUrlQueryString = (url, name) => {
  let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
  let queryStr = url.substr(url.indexOf('?') + 1)
  let result = queryStr.match(reg)
  return result ? decodeURIComponent(result[2]) : null
}

/**
 * 生成随机数
 * @param randomIndicator 位数是否随机
 * @param min 最小位数
 * @param max 最大位数
 * @returns {string} 返回生成的随机数
 */
export const randomWord = (randomIndicator, min, max) => {
  let arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  let str = ''
  let range = min
  if (randomIndicator) {
    range = Math.round(Math.random() * (max - min)) + min
  }
  for (let i = 0; i < range; i++) {
    let pos = Math.round(Math.random() * (arr.length - 1))
    str += arr[pos]
  }
  return str
}

/**
 * 获取当前星期几
 */
export const getWeekDay = (date) => {
  let _date = date || new Date()
  let day = _date.getDay()
  let weeks = new Array('星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六')
  return weeks[day]
}

/**
 * 获取距离当前日期的时间
 */
export const getDateFromToday = (n) => {
  let nowDay = new Date()
  let targetDay = new Date(nowDay.getTime() + n * 24 * 60 * 60 * 1000)

  let tYear = targetDay.getFullYear()
  let tMonth = targetDay.getMonth() + 1
  let tDate = targetDay.getDate()

  return tYear + '-' + zero(tMonth) + '-' + zero(tDate)
}

/**
 * 利用递归方法实现深拷贝
 * 原理：遍历对象、数组直到里边都是基本数据类型，然后再去复制，就是深度拷贝
 */
export const deepClone = (obj, hash = new WeakMap()) => {
  if (obj === null) return obj
  // 如果是null或者undefined我就不进行拷贝操作
  if (obj instanceof Date) return new Date(obj)
  if (obj instanceof RegExp) return new RegExp(obj)
  // 可能是对象或者普通的值  如果是函数的话是不需要深拷贝
  if (typeof obj !== 'object') return obj
  // 是对象的话就要进行深拷贝
  if (hash.get(obj)) return hash.get(obj)
  let cloneObj = new obj.constructor()
  // 找到的是所属类原型上的constructor,而原型上的 constructor指向的是当前类本身
  hash.set(obj, cloneObj)
  for (let key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      // 实现一个递归拷贝
      cloneObj[key] = deepClone(obj[key], hash)
    }
  }
  return cloneObj
}

/**
 * 判断当前浏览器类型是否为微信浏览器
 */
export const isWeiXin = () => {
  if (navigator.userAgent.toLowerCase().includes('micromessenger')) {
    return true
  } else {
    return false
  }
}

/**
 * 将对象数组按组合算法返回
 * 返回：对象数组的组合
 */
export const getCombination = (arr, index = 0, group = []) => {
  if (!arr || !arr.length) return []

  let tempGroup = []
  tempGroup.push([arr[index]])
  for (let i = 0; i < group.length; i++) {
    tempGroup.push([...group[i], arr[index]])
  }
  group = group.concat(tempGroup)

  if (index + 1 >= arr.length) {
    return group
  } else {
    return getCombination(arr, index + 1, group)
  }
}

/**
 * 01背包问题算法（优化：合并循环）
 * @param {Array} weights 物品重量
 * @param {Array} values 物品价值
 * @param {Number} W 背包总容量
 */
export const knapsack = (weights, values, W) => {
  let n = weights.length
  let f = new Array(n) // 定义f的矩阵
  f[-1] = new Array(W + 1).fill(0)
  let selected = []
  for (let i = 0; i < n; i++) {
    f[i] = [] // 创建当前的二维数组
    for (let j = 0; j <= W; j++) {
      if (j < weights[i]) { // 如果容量不能放下物品0的重量，那么价值为0
        f[i][j] = f[i - 1][j]
      } else { // 否则容量为物品0的价值
        f[i][j] = Math.max(f[i - 1][j], f[i - 1][j - weights[i]] + values[i])
      }
    }
  }

  let j = W, w = 0
  for (let i = n - 1; i >= 0; i--) {
    if (f[i][j] > f[i - 1][j]) {
      selected.push(i)
      // console.log('物品', i, '其重量为', weights[i], '其价格为', values[i])
      j = j - weights[i]
      w += weights[i]
    }
  }
  // console.log('背包最大承重为', W, ' 现在重量为', w, ' 总价值为', f[n-1][W], '包有', selected.reverse())
  return [f[n - 1][W], selected.reverse()]
}

/**
 * 01背包问题优化算法（空间复杂度优化）
 * 空间复杂度优化：二维数组换成一维数组
 * @param {Array} weights 物品重量
 * @param {Array} values 物品价值
 * @param {Number} W 背包总容量
 */
export const knapsackOptimize = (weights, values, W) => {
  let n = weights.length
  let f = new Array(W + 1).fill(0)
  // let selected = new Array(W + 1).fill(new Bitmap(n))
  let selected = new Array(W + 1)
  for (let i = 0; i < selected.length; i++) {
    selected[i] = new Bitmap(n)
  }
  // let selected = new Array(W + 1).fill('')

  for (let i = 1; i <= n; i++) {
    // console.log('n: ', n)
    for (let j = W; j >= weights[i - 1]; j--) {
      let temp = f[j - weights[i - 1]] + values[i - 1]
      if (temp > f[j]) {
        f[j] = temp
        // selected[j] = [...selected[j - weights[i - 1]], i - 1]

        // selected[j] = selected[j - weights[i - 1]] + (i - 1) + ','
        // selected[j] = selected[j - weights[i - 1]] + (i - 1) + (i === n ? '' : ',')
        
        selected[j].copy(selected[j - weights[i - 1]])
        selected[j].set(i - 1)
      }
    }
  }
  // console.log('背包最大承重为', W, ' 总价值为', f[W], ' 包有', selected[W].getAllNum())
  return [f[W], selected[W].getAllNum()]
  // return [f[W], selected[W].split(',').map(v => +v)]
}

/**
 * 求两个数的最大公约数（辗转相除法）
 * @param {Number} a 整数
 * @param {Number} b 整数
 */
export const gcd = (a, b) => {
  return b == 0 ? a : gcd(b, a % b)
}

/**
 * 求一组数中的最大公约数
 * 特殊情况处理：数组为空 或 数组长度为1且元素为0，返回1；数组长度为1且元素不为0时，返回该元素
 * @param {Array} arr 整数数组
 */
export const gcdOfArray = (arr) => {
  let temp = [...new Set(arr)] // 对数组去重，减少重复运算
  let result = temp[0] || 1 // 初始化结果为第一个数，如果没有或第一个数是0时则为1
  while (temp.length > 1) {
    let num1 = temp[0]
    let num2 = temp[1]
    result = gcd(num1, num2) // 求两个数的最大公约数
    if (result === 1) {
      return result // 如果中间出现最大公约数是1，则直接返回
    } else {
      temp.splice(0, 2, result) // 迭代，继续求最大公约数
    }
  }
  return result
}

/**
 * baidu OCR图片压缩
 * 图片大小限制小于4M，分辨率不高于4096*4096
 * @param {FormData} file 文件
 * @param {Function} callback 回调函数
 */
export const imageCompression = (file, callback) => {
  let reader = new FileReader()
  reader.readAsDataURL(file)
  reader.onload = e => {
    let newImg = new Image()
    newImg.src = e.target.result
    let rate = 1
    let imgW
    let imgH
    newImg.onload = () => {
      try {
        // 计算压缩比例
        let maxSide = Math.max(newImg.width, newImg.height)
        if (maxSide > 4096) {
          rate = maxSide / 4096
        } else if (file.size > 4194304) {
          rate = Math.sqrt(file.size / 4194304)
        }
        // 用canvas画图
        imgW = Math.floor(newImg.width / rate)
        imgH = Math.floor(newImg.height / rate)
        let canvas = document.createElement('canvas')
        let ctx = canvas.getContext('2d')
        canvas.width = imgW
        canvas.height = imgH
        // 图片压缩
        ctx.drawImage(newImg, 0, 0, newImg.width, newImg.height, 0, 0, imgW, imgH)
        let base64Data = canvas.toDataURL(file.type)
        // // base64转换file
        // let arr = base64Data.split(',')
        // let mime = arr[0].match(/:(.*?);/)[1]
        // let suffix = mime.split('/')[1]
        // let bstr = atob(arr[1])
        // let n = bstr.length
        // let u8arr = new Uint8Array(n)
        // while (n--) {
        //   u8arr[n] = bstr.charCodeAt(n)
        // }
        // let fileData = new File([u8arr], `${file.name}.${suffix}`, { type: mime })

        // // 压缩可能存在误差，若仍大于4兆则继续压缩
        // if (fileData.size > 4194304) {
          
        // } else {
        callback(base64Data)
        // }
      } catch (err) {
        callback(null)
      }
    }
  }
}

/* eslint-enable */
