export class Bitmap {
  /**
   * 创建bitmap数组
   * @param {Number} size 个数
   */
  constructor (size) {
    this.size = size
    this.bin = new Uint8Array(new ArrayBuffer((size >> 3) + 1))
  }

  /**
   * num/8得到byte[]的index
   * @param {Number} num 整数
   */
  getIndex (num) {
    return num >> 3
  }

  /**
   * num%8得到在byte[index]的位置
   * @param {Number} num 整数
   */
  getPosition (num) {
    // return num % 8
    return num & 0x07
  }

  /**
   * 标记指定数字（num）在bitmap中的值，标记其已经出现过
   * 将1左移position后，那个位置自然就是1，然后和以前的数据做|，这样，那个位置就替换成1了
   * @param {Number} num 整数
   */
  set (num) {
    this.bin[this.getIndex(num)] |= 1 << this.getPosition(num)
  }

  /**
   * 判断指定数字num是否存在
   * 将1左移position后，那个位置自然就是1，然后和以前的数据做&，判断是否为0即可
   * @param {Number} num 整数
   */
  contains (num) {
    return (this.bin[this.getIndex(num)] & 1 << this.getPosition(num)) !== 0
  }

  /**
   * 重置某一数字对应在bitmap中的值
   * 对1进行左移，然后取反，最后与byte[index]作与操作
   * @param {Number} num 整数
   */
  clear (num) {
    this.bin[this.getIndex(num)] &= ~(1 << this.getPosition(num))
  }

  clearAll () {
    this.bin.fill(0)
  }

  add (bMap) {
    for (let i = 0; i < this.bin.length; i++) {
      this.bin[i] |= bMap.bin[i]
    }
  }

  copy (newBmap) {
    // for (let i = 0; i < this.size; i++) {
    //   if (this.contains(i)) {
    //     bMap.set(i)
    //   } else {
    //     bMap.clear(i)
    //   }
    // }
    // this.clearAll()
    // this.add(nBmap)
    this.clearAll()
    // let zeroBmap = new Uint8Array(new ArrayBuffer(1))
    for (let i = 0; i < this.bin.length; i++) {
      // this.bin[i] &= zeroBmap[0]
      this.bin[i] |= newBmap.bin[i]
    }
    // let zeroBmap = new Uint8Array(new ArrayBuffer(1))
    // console.log(zeroBmap[0])
    // this.bin &= zeroBmap[0]
    // this.bin |= newBmap.bin
  }

  /**
   * 获取所有存在的数字
   */
  getAllNum () {
    let result = []
    for (let i = 0; i < this.size; i++) {
      if (this.contains(i)) {
        result.push(i)
      }
    }
    return result
  }

  // get (index) {
  //   const row = index >> 3
  //   const bit = 1 << (index % 8)
  //   return (this.bin[row] & bit) > 0
  // }

  // set (index, bool = true) {
  //   const row = index >> 3
  //   const bit = 1 << (index % 8)
  //   if (bool) {
  //     this.bin[row] |= bit
  //   } else {
  //     this.bin[row] &= (255 ^ bit)
  //   }
  // }

  // fill (num = 0) {
  //   this.bin.fill(num)
  // }

  // flip (index) {
  //   const row = Math.floor(index / 8)
  //   const bit = 1 << (index % 8)
  //   this.bin[row] ^= bit
  // }
}
