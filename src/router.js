import Vue from 'vue'
import Router from 'vue-router'
import { Toast } from 'vant'

Vue.use(Router)

const scrollBehavior = (to, from, savedPosition) => {
  // 返回页面跳转后记录的浏览位置
  if (savedPosition && to.meta.keepAlive) {
    return savedPosition
  } else if (savedPosition && to.meta.recordLocation) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(savedPosition)
      }, 500)
    })
  }
  // 异步滚动操作
  return new Promise(resolve => {
    setTimeout(() => {
      resolve({ x: 0, y: 1 })
    }, 0)
  })
}

const vueRouter = new Router({
  mode: 'history',
  scrollBehavior,
  base: process.env.BASE_URL,
  routes: [
    {
      // 首页
      path: '/',
      name: 'Index',
      meta: {
        title: '动保商城首页',
        keywords: '网上购物，网上商城...',
        description: '动保商城...',
        // keepAlive: false
        recordLocation: false
      },
      component: () => import('@/views/index-2.vue')
    },
    {
      // 更多分类
      path: '/moreCategories',
      name: 'moreCategories',
      meta: {
        title: '更多分类'
      },
      component: () => import('@/views/more-categories-2.vue')
    },
    {
      // 商品详情
      path: '/goodsDetails',
      name: 'goodsDetails',
      meta: {
        title: '商品详情'
      },
      component: () => import('@/views/goods-details/goods-details.vue')
    },
    {
      // 播放视频
      path: '/videoPage',
      name: 'videoPage',
      meta: {
        title: '视频播放'
      },
      component: () => import('@/views/goods-details/video-page.vue')
    },
    {
      // 购物车
      path: '/shoppingCart',
      name: 'shoppingCart',
      meta: {
        title: '购物车',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/shopping-cart.vue')
    },
    {
      // 提交订单
      path: '/placeOrder',
      name: 'placeOrder',
      meta: {
        title: '提交订单',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/place-order/place-order.vue')
    },
    {
      // 收货地址
      path: '/receivingAddress',
      name: 'receivingAddress',
      meta: {
        title: '收货地址',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/place-order/receiving-address.vue')
    },
    {
      // 司机信息
      path: '/driver',
      name: 'driver',
      meta: {
        title: '司机信息',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/place-order/driver.vue')
    },
    {
      //客户列表
      path: '/customersList',
      name: 'customersList',
      meta: {
        title: '客户列表'
      },
      component: () => import('@/views/customers-List.vue')
    },
    {
      // 商品列表
      path: '/goodsList',
      name: 'goodsList',
      meta: {
        title: '商品列表'
      },
      component: () => import('@/views/goods-list.vue')
    },
    {
      // 店铺页
      path: '/shop',
      name: 'shop',
      meta: {
        title: '店铺',
        recordLocation: false
      },
      component: () => import('@/views/shop.vue')
    },
    {
      // 快速下单页
      path: '/quickOrder',
      name: 'quickOrder',
      meta: {
        title: '快速下单',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/quick-order.vue')
    },
    {
      // 订单页
      path: '/order',
      name: 'order',
      meta: {
        title: '我的订单',
        // keepAlive: false
        requireLogin: false, // 判断是否需要登录
        recordLocation: false,
        driverCanVisit: false // 司机只能访问的页面
      },
      component: () => import('@/views/order/order.vue')
    },
    {
      // 订单子页
      path: '/orderIndex',
      name: 'orderIndex',
      meta: {
        title: '订单',
        requireLogin: false, // 判断是否需要登录
        driverCanVisit: false // 司机只能访问的页面
      },
      component: () => import('@/views/order/index.vue'),
      children: [
        {
          // 订单-订单详情页
          path: '/orderDetail',
          name: 'orderDetail',
          meta: {
            title: '订单-订单详情',
            driverCanVisit: false // 司机只能访问的页面
          },
          component: () => import('@/views/order/child/order-detail.vue')
        },
        {
          // 订单-物流
          path: '/logistics',
          name: 'logistics',
          meta: {
            title: '订单-物流'
          },
          component: () => import('@/views/order/child/logistics.vue')
        },
        {
          // 订单-评价
          path: '/evaluate',
          name: 'evaluate',
          meta: {
            title: '订单-评价'
          },
          component: () => import('@/views/order/child/evaluate.vue')
        },
        {
          // 订单-评价列表
          path: '/evaluateList',
          name: 'evaluateList',
          meta: {
            title: '订单-评价列表'
          },
          component: () => import('@/views/order/child/evaluate-list.vue')
        },
        {
          // 订单-售后申请列表
          path: '/applyList',
          name: 'applyList',
          meta: {
            title: '订单-售后申请列表'
          },
          component: () => import('@/views/order/child/apply-list.vue')
        },
        {
          // 订单-售后申请
          path: '/afterSale',
          name: 'afterSale',
          meta: {
            title: '订单-售后申请'
          },
          component: () => import('@/views/order/child/after-sale.vue')
        },
        {
          // 订单-确认订单(养猪业务)
          path: '/confirmOrder',
          name: 'confirmOrder',
          meta: {
            title: '订单-确认订单'
          },
          component: () => import('@/views/order/child/confirm-order.vue')
        }
      ]
    },
    {
      // 登录
      path: '/login',
      name: 'login',
      meta: {
        title: '登录'
      },
      component: () => import('@/views/login/login.vue')
    },
    {
      // 登录-短信验证码
      path: '/loginRandom',
      name: 'loginRandom',
      meta: {
        title: '登录'
      },
      component: () => import('@/views/login/login-random.vue')
    },
    {
      // 忘记密码
      path: '/forget',
      name: 'forget',
      meta: {
        title: '忘记密码'
      },
      component: () => import('@/views/login/forget.vue')
    },
    // {
    //   // 忘记密码-获取验证码
    //   path: '/forgetValidate',
    //   name: 'forgetValidate',
    //   meta: {
    //     title: '忘记密码-获取验证码'
    //   },
    //   component: () => import('@/views/login/forget-validate.vue')
    // },
    {
      // 忘记密码-重置密码
      path: '/forgetReset',
      name: 'forgetReset',
      meta: {
        title: '忘记密码-重置密码'
      },
      component: () => import('@/views/login/forget-reset.vue')
    },
    {
      // 个人注册
      path: '/register',
      name: 'register',
      meta: {
        title: '个人注册'
      },
      component: () => import('@/views/register/register.vue')
    },
    {
      // 注册规则
      path: '/registerRule',
      name: 'registerRule',
      meta: {
        title: '注册规则'
      },
      component: () => import('@/views/register/register-rule.vue')
    },
    {
      // 个人注册-账号信息
      path: '/registerInfo',
      name: 'registerInfo',
      meta: {
        title: '个人注册-账号信息'
      },
      component: () => import('@/views/register/register-info.vue')
    },
    {
      // 个人注册-个人信息
      path: '/registerPersonalInfo',
      name: 'registerPersonalInfo',
      meta: {
        title: '个人注册-个人信息'
      },
      component: () => import('@/views/register/register-personal-info.vue')
    },
    {
      // 个人注册-销售部门选择
      path: '/salesChoose',
      name: 'salesChoose',
      meta: {
        title: '个人注册-销售部门选择'
      },
      component: () => import('@/views/register/sales-choose.vue')
    },
    {
      // 个人注册-用户类型选择
      path: '/userChoose',
      name: 'userChoose',
      meta: {
        title: '个人注册-用户类型选择'
      },
      component: () => import('@/views/register/user-choose.vue')
    },
    {
      // 个人注册-提交审核
      path: '/registerAuditing',
      name: 'registerAuditing',
      meta: {
        title: '个人注册-提交审核'
      },
      component: () => import('@/views/register/register-auditing.vue')
    },
    {
      // 企业注册
      path: '/companyRegister',
      name: 'companyRegister',
      meta: {
        title: '企业注册'
      },
      component: () => import('@/views/register/company-register.vue')
    },
    {
      // 企业注册-账号信息
      path: '/companyRegisterInfo',
      name: 'companyRegisterInfo',
      meta: {
        title: '企业注册-账号信息'
      },
      component: () => import('@/views/register/company-register-info.vue')
    },
    {
      // 企业注册-公司信息
      path: '/registerCompanyInfo',
      name: 'registerCompanyInfo',
      meta: {
        title: '企业注册-公司信息'
      },
      component: () => import('@/views/register/register-company-info.vue')
    },
    {
      // 企业注册-提交审核
      path: '/companyRegisterAuditing',
      name: 'companyRegisterAuditing',
      meta: {
        title: '企业注册-提交审核'
      },
      component: () => import('@/views/register/company-register-auditing.vue')
    },
    {
      // 自助开户-注册协议展示
      path: '/account',
      name: 'account',
      meta: {
        title: '用户注册'
      },
      component: () => import('@/views/customer-account/account.vue')
    },
    {
      // 自助开户-注册协议和隐私条款
      path: '/registerAgreement',
      name: 'registerAgreement',
      meta: {
        title: '用户注册-注册协议和隐私条款'
      },
      component: () => import('@/views/customer-account/register-agreement.vue')
    },
    {
      // 自助开户-承诺书
      path: '/undertaking',
      name: 'undertaking',
      meta: {
        title: '用户注册-承诺书'
      },
      component: () => import('@/views/customer-account/undertaking.vue')
    },
    {
      // 自助开户-用户（个人/企业）开户
      path: '/userAccount',
      name: 'userAccount',
      meta: {
        title: '用户注册'
      },
      component: () => import('@/views/customer-account/user-account.vue')
    },
    {
      // 自助开户-选择四级公司
      path: '/chooseFourCustomer',
      name: 'chooseFourCustomer',
      meta: {
        title: '用户注册-选择四级公司'
      },
      component: () => import('@/views/customer-account/choose-four-customer.vue')
    },
    {
      // 自助开户-选择开户行
      path: '/chooseOpeningBank',
      name: 'chooseOpeningBank',
      meta: {
        title: '用户注册-选择四级公司'
      },
      component: () => import('@/views/customer-account/choose-opening-bank.vue')
    },
    {
      // 我的-代扣协议-新增
      path: '/withholdAgreementAdd',
      name: 'withholdAgreementAdd',
      meta: {
        title: '代扣协议-新增',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/customer-account/withhold-agreement-add.vue')
    },
    {
      // 我的-代扣协议
      path: '/withholdAgreement',
      name: 'withholdAgreement',
      meta: {
        title: '我的-代扣协议',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/customer-account/withhold-agreement.vue')
    },

    {
      // 我的-代扣协议-查看电子签
      path: '/withholdAgreementEsignView',
      name: 'withholdAgreementEsignView',
      meta: {
        title: '我的-代扣协议-电子签'
        // requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/customer-account/withhold-agreement-esign-view.vue')
    },
    {
      // 我的-会员中心
      path: '/membershipCenter',
      name: 'membershipCenter',
      meta: {
        title: '我的',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/membership-center.vue')
    },
    {
      // 我的-会员中心-资料设置
      path: '/setInfo',
      name: 'setInfo',
      meta: {
        title: '我的-会员中心-资料设置',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/set-info.vue')
    },
    {
      // 我的-会员中心-个人信息&企业信息
      path: '/customerInfo',
      name: 'customerInfo',
      meta: {
        title: '我的-会员中心-个人信息',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-info')
    },
    {
      // 我的-会员中心-个人信息&企业信息
      path: '/switchingCustomers',
      name: 'switchingCustomers',
      meta: {
        title: '我的-会员中心-客户切换',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/switching-customers')
    },
    {
      // 我的-会员中心-账户安全
      path: '/accountSecurity',
      name: 'accountSecurity',
      meta: {
        title: '我的-会员中心-账户安全',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/account-security.vue')
    },
    {
      // 我的-会员中心-修改密码
      path: '/modifyPassword',
      name: 'modifyPassword',
      meta: {
        title: '我的-会员中心-修改密码',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/modify-password.vue')
    },
    {
      // 我的-会员中心-实名认证
      path: '/certification',
      name: 'certification',
      meta: {
        title: '我的-会员中心-实名认证',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/certification.vue')
    },
    {
      // 我的-会员中心-修改手机号码
      path: '/modifyPhone',
      name: 'modifyPhone',
      meta: {
        title: '我的-会员中心-修改手机号码',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/modify-phone.vue')
    },
    {
      // 我的-会员中心-积分等级
      path: '/integral',
      name: 'integral',
      meta: {
        title: '我的-会员中心-积分等级',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/integral.vue')
    },
    {
      // 我的-会员中心-会员管理
      path: '/membershipManagement',
      name: 'membershipManagement',
      meta: {
        title: '我的-会员中心-会员管理',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/member-level/membership-management.vue')
    },
    {
      // 我的-会员中心-会员管理-会员权益说明
      path: '/levelInterestsStatement',
      name: 'levelInterestsStatement',
      meta: {
        title: '我的-会员中心-会员管理',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/member-level/level-interests-statement.vue')
    },
    {
      // 我的-会员中心-我的合约
      path: '/myAgreement',
      name: 'myAgreement',
      meta: {
        title: '我的-会员中心-我的合约',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/my-agreement.vue')
    },
    {
      // 我的-优惠券
      path: '/coupon',
      name: 'coupon',
      meta: {
        title: '我的-优惠券',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/coupon.vue')
    },
    {
      // 我的-历史访问
      path: '/history',
      name: 'history',
      meta: {
        title: '我的-历史访问',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/history.vue')
    },
    {
      // 我的-关注
      path: '/myConcern',
      name: 'myConcern',
      meta: {
        title: '我的-关注',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/concern/my-concern.vue')
    },
    {
      // 我的-销售部申请
      path: '/customerApplication',
      name: 'customerApplication',
      meta: {
        title: '我的-销售部申请',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-application.vue')
    },
    {
      // 我的-账户审核查询
      path: '/verify',
      name: 'verify',
      meta: {
        title: '我的-账户审核查询',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/verify.vue')
    },
    {
      // 我的-退换货管理
      path: '/returnManagement',
      name: 'returnManagement',
      meta: {
        title: '我的-退换货管理',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/return/return-management.vue')
    },
    {
      // 我的-退换货详情
      path: '/returnDetails',
      name: 'returnDetails',
      meta: {
        title: '我的-退换货详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/return/return-details.vue')
    },
    {
      // 我的-钱包
      path: '/wallet',
      name: 'wallet',
      meta: {
        title: '我的-我的钱包',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/wallet.vue')
    },
    {
      // 我的-资产中心
      path: '/fund',
      name: 'fund',
      meta: {
        title: '我的-资产中心',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/fund/fund.vue')
    },
    {
      // 我的-资产中心详情
      path: '/fundDetails',
      name: 'fundDetails',
      meta: {
        title: '我的-资产中心详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/fund/fund-details.vue')
    },
    {
      // 我的-资产中心-充值中心
      path: '/topUp',
      name: 'topUp',
      meta: {
        title: '我的-资产中心-充值中心',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/fund/top-up.vue')
    },
    {
      // 我的-资产中心-保证金明细
      path: '/bondDetails',
      name: 'bondDetails',
      meta: {
        title: '我的-资产中心-保证金明细',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bond-details.vue')
    },
    {
      // 我的-资产中心-保证金退还申请
      path: '/bondReturnApplication',
      name: 'bondReturnApplication',
      meta: {
        title: '我的-资产中心-保证金退还申请',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bond-return-application.vue')
    },
    {
      // 我的-资产中心-销售折扣详情
      path: '/salesDiscountDetails',
      name: 'salesDiscountDetails',
      meta: {
        title: '我的-资产中心-销售折扣详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/fund/sales-discount-details.vue')
    },
    {
      // 我的-客服中心
      path: '/callCenter',
      name: 'callCenter',
      meta: {
        title: '我的-客服中心',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/call-center.vue')
    },
    // {
    //   // 我的-帮助中心
    //   path: '/helpCenter',
    //   name: 'helpCenter',
    //   meta: {
    //     title: '我的-帮助中心',
    //     requireLogin: false // 判断是否需要登录
    //   },
    //   component: () => import('@/views/membership-center/help-center.vue')
    // },
    {
      // 我的-帮助中心
      path: '/helpCenter',
      name: 'helpCenter',
      meta: {
        title: '我的-帮助中心'
      },
      component: () => import('@/views/help-center/help-center.vue')
    },
    {
      // 我的-帮助中心详情
      path: '/helpCenterDetails',
      name: 'helpCenterDetails',
      meta: {
        title: '我的-帮助中心详情'
      },
      component: () => import('@/views/help-center/help-center-details.vue')
    },
    {
      // 我的-意见反馈
      path: '/customerFeedback',
      name: 'customerFeedback',
      meta: {
        title: '我的-意见反馈',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-feedback/customer-feedback.vue')
    },
    {
      // 我的-意见反馈-类型选择
      path: '/customerFeedbackSwitchType',
      name: 'customerFeedbackSwitchType',
      meta: {
        title: '我的-意见反馈-类型选择',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-feedback/customer-feedback-switch-type.vue')
    },
    {
      // 我的-意见反馈详情
      path: '/customerFeedbackDetails',
      name: 'customerFeedbackDetails',
      meta: {
        title: '我的-意见反馈详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-feedback/customer-feedback-details.vue')
    },
    {
      // 我的-降价申请
      path: '/priceReduceApply',
      name: 'priceReduceApply',
      meta: {
        title: '我的-降价申请',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-feedback/price-reduce-apply.vue')
    },
    {
      // 我的-降价申请详情
      path: '/priceReduceApplyDetails',
      name: 'priceReduceApplyDetails',
      meta: {
        title: '我的-降价申请详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/membership-center/customer-feedback/price-reduce-apply-details.vue')
    },
    {
      // 我的-调查问卷
      path: '/questionnaire',
      name: 'questionnaire',
      meta: {
        title: '我的-调查问卷',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/questionnaire/questionnaire.vue')
    },
    {
      // 我的-调查问卷详情
      path: '/questionnaireDetails',
      name: 'questionnaireDetails',
      meta: {
        title: '我的-调查问卷详情'
      },
      component: () => import('@/views/questionnaire/questionnaire-details.vue')
    },
    {
      // 我的-月度需求
      path: '/monthDemand',
      name: 'monthDemand',
      meta: {
        title: '我的-月度需求',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/raisePig/month-demand.vue')
    },
    {
      // 我的-新建月度需求
      path: '/newMonthDemand',
      name: 'newMonthDemand',
      meta: {
        title: '月度需求-新建月度需求',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/raisePig/new-month-demand.vue')
    },
    {
      // 我的-月度需求详情
      path: '/monthDemandDetails',
      name: 'monthDemandDetails',
      meta: {
        title: '月度需求-月度需求详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/raisePig/month-demand-details.vue')
    },
    {
      // 促销
      path: '/activity',
      name: 'activity',
      meta: {
        title: '促销'
      },
      component: () => import('@/views/activity.vue')
    },
    {
      // 自助发货-列表页
      path: '/selfServiceDeliveryList',
      name: 'selfServiceDeliveryList',
      meta: {
        title: '自助发货-列表',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/self-service-delivery/list.vue')
    },
    {
      // 自助发货-详情页
      path: '/selfServiceDeliveryDetail',
      name: 'selfServiceDeliveryDetail',
      meta: {
        title: '自助发货-详情',
        requireLogin: false, // 判断是否需要登录
        driverCanVisit: false // 司机只能访问的页面
      },
      component: () => import('@/views/self-service-delivery/detail.vue')
    },
    {
      // 自助发货-付款页
      path: '/selfServiceDeliveryPayment',
      name: 'selfServiceDeliveryPayment',
      meta: {
        title: '自助发货-付款',
        requireLogin: false, // 判断是否需要登录
        driverCanVisit: false // 司机只能访问的页面
      },
      component: () => import('@/views/self-service-delivery/payment.vue')
    },
    {
      // 自助发货-受托人页
      path: '/selfServiceDeliveryDelegation',
      name: 'selfServiceDeliveryDelegation',
      meta: {
        title: '自助发货-受托人',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/self-service-delivery/delegation.vue')
    },
    {
      // 竞价-正在疯抢
      path: '/robbing',
      name: 'robbing',
      meta: {
        title: '竞价-正在疯抢'
      },
      component: () => import('@/views/bidding/robbing.vue')
    },
    {
      // 竞价-竞价预告
      path: '/biddingNotice',
      name: 'biddingNotice',
      meta: {
        title: '竞价-竞价预告'
      },
      component: () => import('@/views/bidding/bidding-notice.vue')
    },
    // {
    //   // 竞价-竞价历史
    //   path: '/biddingHistory',
    //   name: 'biddingHistory',
    //   meta: {
    //     title: '竞价-竞价历史'
    //   },
    //   component: () => import('@/views/bidding/bidding-history.vue')
    // },
    {
      // 竞价-竞价详情
      path: '/biddingDetails',
      name: 'biddingDetails',
      meta: {
        title: '竞价-竞价详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-details.vue')
    },
    {
      // 竞价-竞价(鸡苗)详情
      path: '/biddingChickDetails',
      name: 'biddingChickDetails',
      meta: {
        title: '竞价-竞价详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-chick-details.vue')
    },
    {
      // 竞价-竞价详情地图
      path: '/biddingDetailsMap',
      name: 'biddingDetailsMap',
      meta: {
        title: '竞价-竞价详情地图',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-details-map.vue')
    },
    {
      // 竞价-我的竞价
      path: '/biddingRecord',
      name: 'biddingRecord',
      meta: {
        title: '我的-我的竞价',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-record.vue')
    },
    {
      // 竞价-竞价明细
      path: '/biddingRecordDetails',
      name: 'biddingRecordDetails',
      meta: {
        title: '我的-我的竞价-竞价明细',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-record-details.vue')
    },
    {
      // 竞价-出价记录
      path: '/biddingOffer',
      name: 'biddingOffer',
      meta: {
        title: '竞价-出价记录',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-offer.vue')
    },
    {
      // 竞价-竞价规则
      path: '/biddingRules',
      name: 'biddingRules',
      meta: {
        title: '竞价-竞价规则',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-rules.vue')
    },
    {
      // 竞价-竞价电子签
      path: '/biddingEsign',
      name: 'biddingEsign',
      meta: {
        title: '竞价-竞价电子签',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-esign.vue')
    },
    {
      // 竞价-竞价电子签详情
      path: '/biddingEsignDetails',
      name: 'biddingEsignDetails',
      meta: {
        title: '竞价-竞价电子签详情'
        // requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-esign-details.vue')
    },
    {
      // 竞价-保证金协议
      path: '/marginAgreement',
      name: 'marginAgreement',
      meta: {
        title: '竞价-保证金协议',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/margin-agreement.vue')
    },
    {
      // 竞价-竞价须知
      path: '/notesBidding',
      name: 'notesBidding',
      meta: {
        title: '竞价-竞价须知',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/notes-bidding.vue')
    },
    {
      // 竞价-卖家承诺
      path: '/sellerPromise',
      name: 'sellerPromise',
      meta: {
        title: '竞价-卖家承诺',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/seller-promise.vue')
    },
    {
      // 竞价-中标公告
      path: '/biddingWinNotice',
      name: 'biddingWinNotice',
      meta: {
        title: '竞价-中标公告',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-win-notice.vue')
    },
    {
      // 竞价-中标公告详情
      path: '/biddingWinNoticeDetails',
      name: 'biddingWinNoticeDetails',
      meta: {
        title: '竞价-中标公告详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/bidding/bidding-win-notice-details.vue')
    },
    {
      // 拼团-拼团列表
      path: '/groupBuying',
      name: 'groupBuying',
      meta: {
        title: '拼团',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/group-buying/group-buying.vue')
    },
    {
      // 拼团-拼团详情-正常参与拼团
      path: '/groupBuyingDetails',
      name: 'groupBuyingDetails',
      meta: {
        title: '拼团详情'
      },
      component: () => import('@/views/group-buying/group-buying-details.vue')
    },
    {
      // 拼团-提交订单
      path: '/groupBuyingPlaceOrder',
      name: 'groupBuyingPlaceOrder',
      meta: {
        title: '拼团-提交订单',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/group-buying/group-buying-place-order.vue')
    },
    {
      // 秒杀-秒杀列表
      path: '/flashSale',
      name: 'flashSale',
      meta: {
        title: '秒杀'
        // requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/flash-sale/flash-sale.vue')
    },
    {
      // 秒杀-秒杀详情
      path: '/flashSaleDetails',
      name: 'flashSaleDetails',
      meta: {
        title: '秒杀详情'
        // requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/flash-sale/flash-sale-details.vue')
    },
    {
      // 秒杀-秒杀记录
      path: '/flashSaleRecord',
      name: 'flashSaleRecord',
      meta: {
        title: '我的秒杀',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/flash-sale/flash-sale-record.vue')
    },
    {
      // 秒杀-秒杀记录明细
      path: '/flashSaleRecordDetails',
      name: 'flashSaleRecordDetails',
      meta: {
        title: '明细',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/flash-sale/flash-sale-record-details.vue')
    },
    {
      // 首页改造-消息栏
      path: '/news',
      name: 'news',
      meta: {
        title: '消息',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/news/news.vue')
    },
    {
      // 首页改造-消息栏-订货汇总列表
      path: '/orderSummarization',
      name: 'orderSummarization',
      meta: {
        title: '订货汇总',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/news/order-summarization.vue')
    },
    {
      // 首页改造-消息栏-订货汇总详情
      path: '/orderSummarizationDetails',
      name: 'orderSummarizationDetails',
      meta: {
        title: '订货汇总详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/news/order-summarization-details.vue')
    },
    {
      // 通知公告列表
      path: '/notice',
      name: 'notice',
      meta: {
        title: '通知公告',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/news/notice.vue')
    },
    {
      // 通知公告详情
      path: '/noticeDetails',
      name: 'noticeDetails',
      meta: {
        title: '通知公告详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/news/notice-details.vue')
    },
    {
      // 反馈信息列表
      path: '/feedbackMsg',
      name: 'feedbackMsg',
      meta: {
        title: '反馈信息',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/news/feedback-msg.vue')
    },
    {
      // 领券中心
      path: '/preferential',
      name: 'preferential',
      meta: {
        title: '领券中心',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/preferential/preferential.vue')
    },
    {
      // 我的-优惠券
      path: '/myCoupon',
      name: 'myCoupon',
      meta: {
        title: '我的-优惠券',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/preferential/my-coupon.vue')
    },
    {
      // 质量评价
      path: '/qualityEvaluation',
      name: 'qualityEvaluation',
      meta: {
        title: '质量评价',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/qualityEvaluation/index.vue')
    },
    {
      // 物流线路-查询线路
      path: '/logisticsLineList',
      name: 'logisticsLineList',
      meta: {
        title: '查询线路',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/logistics-site/logistics-line-list.vue')
    },
    {
      // 物流线路-物流申请
      path: '/logisticsApply',
      name: 'logisticsApply',
      meta: {
        title: '物流申请',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/logistics-site/logistics-apply.vue')
    },
    {
      // 物流线路-申请列表
      path: '/logisticsApplyList',
      name: 'logisticsApplyList',
      meta: {
        title: '申请列表',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/logistics-site/logistics-apply-list.vue')
    },
    {
      // 物流线路-申请详情
      path: '/logisticsApplyDetails',
      name: 'logisticsApplyDetails',
      meta: {
        title: '申请详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/logistics-site/logistics-apply-details.vue')
    },
    {
      // 营销资讯-资讯列表
      path: '/information',
      name: 'information',
      meta: {
        title: '资讯列表'
      },
      component: () => import('@/views/information/information.vue')
    },
    {
      // 营销资讯-资讯详情
      path: '/informationDetails',
      name: 'informationDetails',
      meta: {
        title: '资讯详情'
      },
      component: () => import('@/views/information/information-details.vue')
    },
    {
      // 特价销售-商品列表
      path: '/specialSaleList',
      name: 'specialSaleList',
      meta: {
        title: '特销商品列表'
      },
      component: () => import('@/views/special-sale/special-sale-list.vue')
    },
    {
      // 特价销售-商品详情
      path: '/specialSaleDetails',
      name: 'specialSaleDetails',
      meta: {
        title: '特销商品详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/special-sale/special-sale-details.vue')
    },
    {
      // 特价销售-记录列表
      path: '/specialSaleRecord',
      name: 'specialSaleRecord',
      meta: {
        title: '特销记录',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/special-sale/special-sale-record.vue')
    },
    {
      // 种猪营销-商品详情页
      path: '/breedingPigDetails',
      name: 'breedingPigDetails',
      meta: {
        title: '商品详情',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/breeding-pig/goods-details.vue')
    },
    {
      // 种猪营销-订单页
      path: '/breedingPigPlaceOrder',
      name: 'breedingPigPlaceOrder',
      meta: {
        title: '提交订单',
        requireLogin: false // 判断是否需要登录
      },
      component: () => import('@/views/breeding-pig/place-order.vue')
    },
    {
      // 空白跳转页
      path: '/jumpPage',
      name: 'jumpPage',
      meta: {
        title: '加载中'
      },
      component: () => import('@/views/jump-page.vue')
    },
    {
      // 404
      path: '*',
      redirect: '/'
    }
  ]
})

vueRouter.onError((error) => {
  const pattern = /Loading chunk (\d)+ failed/g
  const isChunkLoadFailed = error.message.match(pattern)
  // const targetPath = vueRouter.history.pending.fullPath
  if (isChunkLoadFailed) {
    window.location.reload(false)
  }
})

vueRouter.beforeEach((to, from, next) => {
  /** 路由发生变化修改页面title */
  document.title = to.meta.title || '温氏商城'

  /** 路由发生变化修改页面keywords, description */
  let meta = document.getElementsByTagName('meta')
  meta.keywords.content = to.meta.keywords || ''
  meta.description.content = to.meta.description || ''

  if (from.name === null) { // 刷新或打开新页面
    next()
  }

  /** 判断该路由是否需要登录 */
  if (to.matched.some(record => record.meta.requireLogin)) {
    if (sessionStorage.getItem('token')) { // 判断当前的token是否存在登录存入的token
      /** 判断该路由是否能让司机访问 */
      if (sessionStorage.getItem('isDriver') === '1') { // 如果是司机
        if (to.matched.every(record => record.meta.driverCanVisit)) {
          next() // 该路由能让司机访问，正常访问
        } else {
          next({ path: '/order', replace: false, query: { orderType: 0, index: 0, businessIndex: 0, biztypeId: '' } }) // 该路由不能让司机访问，强制跳转至订单页
        }
      } else {
        next()
      }
    } else {
      Toast('请先登录')

      // 跳转登录页不带重定向地址页页面列表
      const noRedirectName = [
        'specialSaleDetails'
      ]
      if (noRedirectName.includes(to.name)) { // 某些页面跳转登录页不带重定向地址
        next({ path: '/loginRandom' })
      } else {
        next({ path: '/loginRandom', query: { redirect: to.fullPath } })
      }
    }
  } else {
    if (sessionStorage.getItem('token')) { // 判断是否登录
      /** 判断该路由是否能让司机访问 */
      if (sessionStorage.getItem('isDriver') === '1') { // 如果是司机
        if (to.matched.every(record => record.meta.driverCanVisit)) {
          next() // 该路由能让司机访问，正常访问
        } else {
          next({ path: '/order', replace: false, query: { orderType: 0, index: 0, businessIndex: 0, biztypeId: '' } }) // 该路由不能让司机访问，强制跳转至订单页
        }
      } else {
        next()
      }
    } else {
      next()
    }
  }
})

export default vueRouter
