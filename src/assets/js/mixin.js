import wx from 'weixin-js-sdk'
import { api } from '@/api'

export const allImgDomainNameMixin = {
  data () {
    return {
      allImgDomainName: null,
      defaultProImg: require('@/assets/images/default2.png'),
      token: null
    }
  },
  created () {
    this.allImgDomainName = sessionStorage.getItem('allImgDomainName')
    this.token = sessionStorage.getItem('token')
  }
}

/**
 * 客服
 * 20190704 by Fanglin Zhang
 */
export const KFMixin = {
  data () {
    return {
      kfURL: '' // 客服跳转地址
    }
  },
  // mounted () {
  //   this.getKFUrl()
  // },
  methods: {
    getKFUrl () {
      // 客服窗口所需参数 edit by 刘桂平 20190702
      let subURL = ''
      if (sessionStorage.getItem('token')) {
        let userInfoJson = sessionStorage.getItem('userInfo')
        let userInfo = JSON.parse(userInfoJson)
        let cname = userInfo.userName
        let ctags = (this.$store.state.channel.currentChannel && this.$store.state.channel.currentChannel.cusServiceSetting && this.$store.state.channel.currentChannel.cusServiceSetting.label) || ''
        let customerToken = userInfo.id
        let cdesc = '温氏商城客户，来自MOB端'
        let userGroup = userInfo.group
        let userNick = userInfo.nickname
        let cphone = userInfo.phone
        let webToken = userInfo.id
        let nonce = Math.random().toString(36).substr(2)
        let timestamp = new Date().getTime()
        let signStr = 'nonce=' + nonce + '&timestamp=' + timestamp + '&web_token=' + webToken + '&7a0627529435458f96fb812da349868b'
        // eslint-disable-next-line
        signStr = hex_sha1(signStr)
        let signature = signStr.toUpperCase()
        subURL = encodeURI(
          '&c_name=' + cname +
          '&c_cf_客户分组=' + userGroup +
          '&c_cf_客户昵称=' + userNick +
          '&c_tags=' + ctags +
          '&customer_token=' + customerToken +
          '&c_desc=' + cdesc +
          '&c_phone=' + cphone +
          '&nonce=' + nonce +
          '&signature=' + signature +
          '&timestamp=' + timestamp +
          '&web_token=' + webToken
        )
        ud({
          customer: {
            c_name: cname,
            c_cf_客户分组: userGroup,
            c_cf_客户昵称: userNick,
            c_tags: ctags,
            customer_token: customerToken,
            c_desc: cdesc,
            c_phone: cphone,
            nonce: nonce,
            signature: signature,
            timestamp: timestamp,
            web_token: webToken
          }
        })
      }
      // 客服跳转地址
      this.kfURL = 'https://wens.s2.udesk.cn/im_client/?web_plugin_id=3238' + subURL
    }
  }
}

/**
 * WX jssdk mixin
 * 20201112 by Fanglin Zhang
 */
export const WXMixin = {
  methods: {
    // 获取配置数据
    getWXConfigData () {
      return new Promise(resolve => {
        const url = /(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent) ? window.entryUrl : window.location.href.split('#')[0]
        api.wxInfo({ url }).then(res => {
          if (res.success) {
            resolve(res.data)
          } else {
            resolve(null)
          }
        }, err => {
          resolve(null)
          console.log(err)
        })
      })
    },
    // 初始化接口
    async initWX (jsApiList, checkJsApiCallback, wxReadyCallback, wxErrorCallback) {
      const data = await this.getWXConfigData()

      if (!data) return

      // 通过config接口注入权限验证配置
      wx.config({
        debug: false, // 开启调试模式，调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: data.appId, // 必填，公众号的唯一标识
        timestamp: data.timestamp, // 必填，生成签名的时间戳
        nonceStr: data.noncestr, // 必填，生成签名的随机串
        signature: data.signature, // 必填，签名
        jsApiList: [
          'checkJsApi',
          ...jsApiList
        ]
      })

      // 通过ready接口处理成功验证
      wx.ready(() => {
        // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。

        // 检查接口是否可用
        wx.checkJsApi({
          jsApiList: [ // 需要检测的JS接口列表
            'checkJsApi',
            ...jsApiList
          ],
          success: res => {
            checkJsApiCallback && checkJsApiCallback(res.checkResult)
          },
          fail: res => {
            console.log('wx checkJsApi fail: ', res)
          }
        })

        wxReadyCallback && wxReadyCallback()
      })

      // 通过error接口处理失败验证
      wx.error(res => {
        // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
        wxErrorCallback && wxErrorCallback(res)
        console.log('wx config error: ', res)
      })
    },
    // 微信分享接口
    WXShare (title, desc, link, imgUrl) {
      // 调用 自定义“分享给朋友”及“分享到QQ”按钮的分享内容
      wx.updateAppMessageShareData({
        title, // 分享标题
        desc, // 分享描述
        link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl, // 分享图标
        success: function () {
          // 设置成功
          console.log('updateAppMessageShareData success')
        }
      })
      // 调用 自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容
      wx.updateTimelineShareData({
        title, // 分享标题
        link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl, // 分享图标
        success: function () {
          // 设置成功
          console.log('updateTimelineShareData success')
        }
      })
    },
    // 扫一扫二维码
    WXScanQRCode (successCallback, failCallback) {
      wx.scanQRCode({
        needResult: 1, // 默认为0，扫描结果由微信处理，1则直接返回扫描结果
        scanType: ['qrCode'], // 可以指定扫二维码还是一维码，默认二者都有 ('barCode')
        success: res => {
          successCallback && successCallback(res.resultStr) // 当needResult为1时，扫码返回的结果 res.resultStr
        },
        fail: res => {
          failCallback && failCallback(res)
          console.log('scanQRCode fail: ', res)
        }
      })
    },
    // 小程序web-view - 获取当前环境
    WXMiniprogramGetEnv (callback) {
      wx.miniProgram.getEnv(res => {
        callback && callback(res.miniprogram) // true
      })
    },
    // 小程序web-view - wx.redirectTo 关闭当前页面，跳转到应用内的某个页面
    WXMiniprogramRedirectTo (url) {
      wx.miniProgram.redirectTo({
        url,
        success: res => {
          console.log('wx.miniProgram.redirectTo success: ', res)
        },
        fail: res => {
          console.log('wx.miniProgram.redirectTo fail: ', res)
        }
      })
    },
    WXMiniprogramNavigateBack (delta = 1, failCallback) {
      wx.miniProgram.navigateBack({
        delta,
        success: res => {
          console.log('wx.miniProgram.navigateBack success: ', res)
        },
        fail: res => {
          failCallback && failCallback(res)
          console.log('wx.miniProgram.navigateBack fail: ', res)
        }
      })
    }
  }
}
