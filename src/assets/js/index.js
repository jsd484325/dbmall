import { IK_ARRAY } from '@/api/config'
import { isArray, getCookie } from '@/utils/common'
import state from '@/store/state'

/**
 * 商品列表接口参数
 * @param params 参数 type Array.
 * @param sort 排序参数 type Array.
 * @param pageNo 当前页
 * @param pageSize 页的数量
 * @returns {{query: {bool: {must: *[]}}, pageNo: number, pageSize: number}}
 */
export function goodsListParams (params = [], sort = [], pageNo = 1, pageSize = 10, isGetWtRange = 0) {
  if (!isArray(params)) {
    console.error('apiParams: params不是一个数组')
    return
  }
  return {
    query: {
      bool: {
        must: [
          {
            term: {
              'isOnSell.keyword': '1'
            }
          }
        ].concat(params)
      }
    },
    sort: [].concat(sort),
    isGetWtRange: isGetWtRange,
    pageNo: pageNo,
    pageSize: pageSize
  }
}

/**
 * 搜索接口参数
 * @param params 参数 type Array.
 * @param sort 排序参数 type Array.
 * @param pageNo 当前页
 * @param pageSize 页的数量
 * @returns {{query: {bool: {must: *[]}}, pageNo: number, pageSize: number}}
 */
export function searchGoodsParams (params, sort = [], pageNo = 1, pageSize = 10, breadcrumb, itemClassList, isGetWtRange = 0, isGetStock = 0, sellDate = '') {
  let reqParams = {
    query: {
      bool: {
        must: [
          {
            term: {
              'isOnSell.keyword': '1'
            }
          }
        ]
      }
    },
    sort: [].concat(sort),
    isGetWtRange: isGetWtRange,
    isGetStock: isGetStock,
    sellDate: sellDate,
    pageNo: pageNo,
    pageSize: pageSize
  }

  if (sessionStorage.getItem('token')) {
    if (state.intoPage === 0 && getCookie('selectedChannel')) {
      reqParams.ownerId = JSON.parse(getCookie('selectedChannel')).channelId
    } else if (state.intoPage === 1 && state.channel.currentChannel && state.channel.currentChannel.channelId) {
      reqParams.ownerId = state.channel.currentChannel.channelId
    }
  }

  let must = reqParams.query.bool.must

  if (params) {
    must.push({
      bool: {
        should: [].concat(searchGoodsKey(params))
      }
    })
  }

  let keys = {}

  if (breadcrumb && breadcrumb.length) {
    breadcrumb.forEach(v => {
      if (v[3]) {
        must.push({
          range: {
            [v[1]]: {
              gte: +v[3],
              lte: +v[4]
            }
          }
        })
      } else {
        let key = v[1] + '.keyword'

        // IK_ARRAY.forEach(item => {
        //   if (v[1] === item || typeof v[2] === 'boolean') {
        //     key = key.split('.')[0]
        //   }
        // })

        must.push({
          term: {
            [key]: v[2]
          }
        })

        keys[key] = 1
      }
    })
  }

  if (sessionStorage.getItem('token') && !keys['ownerId.keyword'] && !keys['ownerName.keyword']) {
    if (state.intoPage === 0 && getCookie('selectedChannel')) {
      must.push({ term: { 'ownerId.keyword': JSON.parse(getCookie('selectedChannel')).channelId } })
    } else if (state.intoPage === 1 && state.channel.currentChannel && state.channel.currentChannel.channelId) {
      must.push({ term: { 'ownerId.keyword': state.channel.currentChannel.channelId } })
    }
  }

  if (itemClassList && itemClassList.length) {
    if (itemClassList.length === 1 && itemClassList[0][1] === 'recommend') {
      must.push({
        term: {
          recommend: true
        }
      })
    } else {
      itemClassList.forEach(v => {
        if (v[1] === 'recommend') {
          must.push({
            term: {
              recommend: true
            }
          })
        } else if (v[1] === '1002') {
          must.push({
            term: {
              newSell: true
            }
          })
        } else {
          must.push({
            term: {
              'itemClassList.keyword': v[1]
            }
          })
        }
      })

      reqParams.itemClassNumber = itemClassList[0][1]
    }
  }

  return reqParams
}
// export function searchGoodsParams(params = [], sort = [], pageNo = 1, pageSize = 10) {
//     if (!isArray(params)) {
//         console.error('apiParams: params不是一个数组');
//         return;
//     }

//     let params = {
//         "query": {
//             "bool": {
//                 "must": [
//                     {
//                         "term": {
//                             "isOnSell.keyword": "1"
//                         }
//                     },
//                     {
//                         "bool": {
//                             "should": [].concat(params)
//                         }
//                     },
//                     {

//                         "term": {
//                             "itemClassList": ["101", "10101"]
//                         }

//                     },
//                     {
//                         "range": {

//                         }
//                     }
//                 ]
//             }
//         },
//         "sort": [].concat(sort),
//         "pageNo": pageNo,
//         "pageSize": pageSize
//     }

//     return params
// }

/**
 * 搜索主键参数
 * @param val 搜索内容
 * @returns {Array}
 */
export function searchGoodsKey (val) {
  let terms = []
  let wildcards = []
  let matchQueryPhrases = []
  for (let i = 0; i < IK_ARRAY.length; i++) {
    terms.push({
      term: {
        [IK_ARRAY[i] + '.keyword']: val
      }
    })
    wildcards.push({
      wildcard: {
        [IK_ARRAY[i]]: val + '*'
      }
    })
    matchQueryPhrases.push({
      match_phrase: {
        [IK_ARRAY[i]]: val
      }
    })
  }
  return [...terms, ...wildcards, ...matchQueryPhrases]
}
