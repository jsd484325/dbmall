import axios from 'axios'
import router from '@/router'
import store from '@/store'
import { Toast } from 'vant'
import { getCookie, setCookie } from '@/utils/common'
// import { api } from '@/api'

// 域名地址
export const DOMAIN_NAME = window.configURL.API_DOMAIN_NAME // 部署地址
// export const DOMAIN_NAME = 'https://ierp.kingdee.com:2024/devb2bwens/'
// export const DOMAIN_NAME = 'http://localhost:8080/ierp/'
// export const DOMAIN_NAME = 'http://172.20.70.82:8080/ierp/'
// export const DOMAIN_NAME = 'http://172.20.70.128:8080/ierp/'
// export const DOMAIN_NAME = 'http://172.16.11.143:8080/ierp/'
// export const DOMAIN_NAME = 'http://172.16.11.94:8080/ierp/'
// export const DOMAIN_NAME = 'http://192.168.179.128:8080/ierp/'
// export const DOMAIN_NAME = 'http://192.168.179.202:7070/ierp/'
// export const DOMAIN_NAME = 'http://193.17.179.79:8080/ierp/'
// export const DOMAIN_NAME = 'http://t-api-mall.wens.com.cn/ierp/' // 开发环境
// export const DOMAIN_NAME = 'https://t-api-mall.wens.com.cn/ierp/' // 开发环境
// export const DOMAIN_NAME = 'https://test-api-mall.wens.com.cn/ierp/' // 测试环境
// export const DOMAIN_NAME = 'http://10.11.48.60:8888/b2b/ierp/' // uat环境1.1
// export const DOMAIN_NAME = 'http://10.11.48.60:9999/b2b/ierp/' // uat环境1.5
// export const DOMAIN_NAME = 'http://10.16.2.164:8081/ierp/'
// export const DOMAIN_NAME = 'http://10.16.2.145:8081/ierp/'
// export const DOMAIN_NAME = 'http://10.11.40.23:8090/ierp/'
// export const DOMAIN_NAME = 'http://10.16.3.43:8081/ierp/'
// export const DOMAIN_NAME = 'http://10.16.3.99:8081/ierp/'
// export const DOMAIN_NAME = 'http://10.16.3.103:8081/ierp/'

// 请求成功状态码
export const ERR_OK = 1

// 分子
// export const IK_PINGYIN_ARRAY = ['title', 'shortTitle', 'searchKey', 'itemClassName', 'ownerName'];
export const IK_ARRAY = ['title']

// axios.defaults.withCredentials = true // 让ajax携带cookie
// axios.defaults.timeout = 60000 // 设置超时时长

// http request 拦截器
axios.interceptors.request.use(
  config => {
    // 当用户登录后给所有的请求添加一个头
    let token = sessionStorage.getItem('token')

    // 与token无关的接口
    // const withoutTokenList = [
    //   // 'getImageFullUrl',
    //   // 'getPublicKey',
    //   // 'itemClassQuery',
    //   // 'adListQuery',
    //   // 'admindivisionQuery',
    //   // 'assistantdataQuery',
    //   // 'itemClassQueryWens',
    //   // 'customerQuery',
    //   // 'yzjlogin', // 平台本身
    //   // 'smsLogin', // 平台本身
    //   // 'wsUserIsExist',
    //   // 'isMdrCustomer',
    //   // 'verifyCode', // 平台本身
    //   // // 'getVerfiyCode',
    //   // 'sendMessage',
    //   // 'verifyCode4Pwd',
    //   // 'getImageCode',
    //   // 'getImageVerify',
    //   // 'wsSendSMS',
    //   // 'wsSMSValidate',
    //   // 'autoJumpUrl'
    // ]

    // let isOnWithoutTokenList = withoutTokenList.some(v => config.url.includes(v))
    // if (token && !isOnWithoutTokenList) {
    if (token) {
      config.headers['Access-Token'] = token
      config.headers.accessToken = token
    }

    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 添加响应拦截器
axios.interceptors.response.use(response => {
  if (response && response.data && 'success' in response.data) {
    response.data.message = response.data.message || response.data.errorCode
    if (response.data.success !== true) {
      response.data.success = false
    }
  }

  const data = response.data

  if (store.state.tokenInvalidFlag && data && ((data.error_code === 1 && data.error_desc && data.error_desc.indexOf('未经授权的访问') > -1) || data.errorcode === '100001')) {
    if (getCookie('user_token')) {
      setCookie('user_token', '', -1)
    }

    sessionStorage.removeItem('token')
    sessionStorage.removeItem('br-custom-key')
    sessionStorage.removeItem('defaultCustomer')
    sessionStorage.removeItem('isDriver')
    sessionStorage.removeItem('groupBuyingCache')
    sessionStorage.removeItem('flashSaleCache')
    sessionStorage.getItem('region') && store.dispatch('setRegion', { data: JSON.parse(sessionStorage.getItem('region')) })

    if (!router.history.current.query.token) {
      router.replace({ path: '/loginRandom' })
      Toast.clear(true)
      setTimeout(() => {
        Toast.fail('登录已过期，请重新登录')
      }, 0)
    } else {
      sessionStorage.setItem('token', router.history.current.query.token)
      router.replace({ path: '/' })
      location.reload(true)
    }

    store.dispatch('setTokenInvalidFlag', false)

    return response
  } else {
    return response
  }
}, error => {
  if (error && error.response) {
    switch (error.response.status) {
      case 400:
        error.message = '请求错误(400)'
        break
      case 401:
        error.message = '未授权，请重新登录(401)'
        break
      case 403:
        error.message = '拒绝访问(403)'
        break
      case 404:
        error.message = '请求出错(404)'
        break
      case 408:
        error.message = '请求超时(408)'
        break
      case 500:
        error.message = '服务器错误(500)'
        break
      case 501:
        error.message = '服务未实现(501)'
        break
      case 502:
        error.message = '网络错误(502)'
        break
      case 503:
        error.message = '服务不可用(503)'
        break
      case 504:
        error.message = '网络超时(504)'
        break
      case 505:
        error.message = 'HTTP版本不受支持(505)'
        break
      default:
        error.message = `连接出错(${error.response.status})!`
    }
  } else {
    error.message = '连接服务器失败!'
  }
  return Promise.reject(error)
})
