import axios from 'axios'
import qs from 'qs'
import { DOMAIN_NAME } from './config'

// axios.defaults.withCredentials = true

// // 记录重复压缩次数
// let compressFlag = 0

// 调用上传图片接口
let uploadImageFun = (file, config, fn) => {
  let formData = new FormData()
  formData.append('file', file)
  httpTwo(DOMAIN_NAME + 'wens/auth/file/uploadImage.do', formData, 'POST', config).then(response => {
    fn(response.data)
  }).catch((error) => {
    fn({
      msg: error.message
    })
    return Promise.reject(error)
  })
}

/**
 * 压缩图片
 * @param {FormData} file 文件
 * @param {Object} config http config
 * @param {Function} fn 回调函数
 */
let photoCompress = (file, config, fn) => {
  if (file.size > 10485760) {
    fn({
      msg: '文件过大，请上传10MB以下的文件'
    })
    return
  }

  if (file.type.includes('image')) { // 判断是否为符合条件的图片格式
    if (!file.type.includes('png') && !file.type.includes('jpeg') && !file.type.includes('jpg')) {
      fn({
        msg: '图片格式不正确，请上传PNG/JPEG/JPG格式的图片'
      })
      return
    }
  } else if (file.type.includes('video')) { // 判断是否为视频格式（mov格式解析为quicktime）
    if (file.type.includes('mp4') || file.type.includes('avi') || file.type.includes('quicktime')) {
      uploadImageFun(file, config, fn)
      return
    } else {
      fn({
        msg: '视频格式不正确，请上传MP4/AVI/MOV格式的视频'
      })
      return
    }
  }

  if (file.size <= 5242880) {
    uploadImageFun(file, config, fn)
    return
  }

  // compressFlag++
  // file转换url
  var reader = new FileReader()
  reader.readAsDataURL(file)
  reader.onload = e => {
    // 计算压缩比例，默认控制5兆
    var rate = Math.sqrt(file.size / 5242880)
    var newImg = new Image()
    newImg.src = e.target.result
    var imgW
    var imgH
    newImg.onload = () => {
      try {
        // 用canvas画图
        imgW = Math.floor(newImg.width / rate)
        imgH = Math.floor(newImg.height / rate)
        var canvas = document.createElement('canvas')
        var ctx = canvas.getContext('2d')
        canvas.width = imgW
        canvas.height = imgH
        // 图片压缩
        ctx.drawImage(newImg, 0, 0, newImg.width, newImg.height, 0, 0, imgW, imgH)
        var base64Data = canvas.toDataURL(file.type)
        // // base64转换blob
        // var arr = base64Data.split(',')
        // var mime = arr[0].match(/:(.*?);/)[1]
        // var bstr = atob(arr[1])
        // var n = bstr.length
        // var u8arr = new Uint8Array(n)
        // while (n--) {
        //   u8arr[n] = bstr.charCodeAt(n)
        // }
        // var blobData = new Blob([u8arr], { type: mime })
        // base64转换file
        let arr = base64Data.split(',')
        let mime = arr[0].match(/:(.*?);/)[1]
        let suffix = mime.split('/')[1]
        let bstr = atob(arr[1])
        let n = bstr.length
        let u8arr = new Uint8Array(n)
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n)
        }
        let fileData = new File([u8arr], `${file.name}.${suffix}`, { type: mime })
        // // 压缩可能存在误差，若仍大于5兆则继续压缩，默认最多3次
        // if (fileData.size > 5242880) {
        //   if (compressFlag > 3) {
        //     compressFlag = 0
        //     fn({
        //       msg: '上传图片失败'
        //     })
        //   } else {
        //     photoCompress(fileData, config, fn)
        //   }
        // } else {
        // compressFlag = 0
        uploadImageFun(fileData, config, fn)
        // }
      } catch (err) {
        fn({
          msg: '图片超出限定尺寸，请重新上传'
        })
      }
    }
  }
}

// 用于格式化“供货授权关系查询”返回的参数
let formatList = (list) => {
  if (list && list.length) {
    let returnList = []

    list.forEach(v => {
      let returnItem = {}

      returnItem.id = v.id
      returnItem.channelId = v.id
      returnItem.channelName = v.name
      returnItem.channelType = v.type
      returnItem.channelAreaName = v.channelAreaName || '其它'
      returnItem.salesAreaName = v.salesAreaName || '其它'
      returnItem.storeName = v.storeName
      returnItem.billStatus = 'C'
      returnItem.cusServiceSetting = v.cusServiceSetting || {}
      returnItem.defaultPaymentChannelId = v.defaultPaymentChannelId || null

      returnList.push(returnItem)
    })

    return returnList
  }
  return list
}

const api = {
  /** 登录/登出/注册等 */
  // 判断用户是否已存在
  userIsExist (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.phone)
      postObj.phone = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('wsUserIsExist', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 判断用户是否有效
  isMdrCustomer (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.phone)
      postObj.phone = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('isMdrCustomer', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取登录图形验证码
  verifyCode (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'wens/auth/login/getVerifyCode.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取登录短信验证码
  sendMessage (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'wens/auth/login/sendMessage.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 验证短信验证码
  verifyCode4Pwd (obj = {}) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedMobile = RSAUtils.encryptedString(key, obj.mobile)
      postObj.mobile = encrypedMobile
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('verifyCode4Pwd', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取公钥
  getKeys () {
    return http('getPublicKey', {}, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取苍穹公钥
  getCqPublicKey (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'auth/getPublicKey.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取苍穹数据中心
  getAllDatacenters (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'auth/getAllDatacenters.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 重置密码/忘记密码验证——获取图形验证码
  getVerifyCode (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'auth/getVerifyCode.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 重置密码/忘记密码验证——验证图形验证码
  verifyCode4PwdCq (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'auth/verifyCode4Pwd.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 重置密码/忘记密码验证——发送短信验证码
  verifyPhone (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'auth/verifyPhone.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 重置密码/忘记密码验证——验证短信验证码
  verifyPhoneCode (obj = {}) {
    return httpTwo(DOMAIN_NAME + 'auth/verifyPhoneCode.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 重置密码/忘记密码——苍穹
  resetPassword (obj) {
    return httpTwo(DOMAIN_NAME + 'auth/resetPassword.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 登录
  login (obj) {
    if (obj.loginType === '2') { // 账号密码登录
      return httpTwo(DOMAIN_NAME + 'auth/yzjlogin.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
        return Promise.resolve(response.data)
      }).catch((error) => {
        return Promise.reject(error)
      })
    } else if (obj.loginType === '1') { // 短信验证码登录
      return httpTwo(DOMAIN_NAME + 'auth/smsLogin.do', qs.stringify(obj), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
        return Promise.resolve(response.data)
      }).catch((error) => {
        return Promise.reject(error)
      })
    }
  },
  // 登出
  logout () {
    let accessToken = sessionStorage.getItem('token')
    // return httpGet(DOMAIN_NAME + 'api/logout.do', { access_token: accessToken }).then(response => {
    return httpTwo(DOMAIN_NAME + 'api/logout.do', qs.stringify({ access_token: accessToken }), 'POST', { 'Content-Type': 'application/x-www-form-urlencoded' }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 修改密码
  changePsw (obj, options = {}) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPwd = RSAUtils.encryptedString(key, obj.passwordChange)
      delete postObj.passwordChange
      postObj.cryptPassword = encrypedPwd
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('changeUserPassword', postObj, 'POST', options).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 绑定手机号码
  bindingPhone (obj) {
    // let postObj = { ...obj }
    // let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    // if (wsmallPublickeyData) {
    //   let keyPair = JSON.parse(wsmallPublickeyData)
    //   RSAUtils.setMaxDigits(200)
    //   const key = RSAUtils.getKeyPair(
    //     keyPair.publicKeyExponent,
    //     '',
    //     keyPair.publicKeyModulus
    //   )
    //   const encrypedPhone = RSAUtils.encryptedString(key, obj.phone)
    //   postObj.phone = encrypedPhone
    //   postObj.keyCode = keyPair.publicKeyCode
    // }
    return http('userInfoModify', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 发送验证码（单独）
  wsSendSMS (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.account)
      postObj.account = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }

    return http('wsSendSMS', postObj, 'POST').then(response => {
      let res = null
      if (response.data.success) {
        res = response.data
      } else {
        let msg = ''
        try {
          if (response.data[0].p[0].errorInfo && /^java.lang.Exception: (.+)！\r*\n\t/.test(response.data[0].p[0].errorInfo)) {
            msg = RegExp.$1
          } else {
            msg = '验证码发送失败'
          }
        } catch (err) {
          msg = '验证码发送失败'
        }
        res = { msg }
      }

      return Promise.resolve(res)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 验证手机号码（单独）
  wsSMSValidate (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.account)
      postObj.account = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('wsSMSValidate', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取验证图片（单独）
  getImageCode (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.phoneNumber)
      postObj.phoneNumber = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('getImageCode', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 图形码验证（单独）
  getImageVerify (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.phoneNumber)
      postObj.phoneNumber = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('getImageVerify', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取可允许输错密码总次数
  getLockCount (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPhone = RSAUtils.encryptedString(key, obj.phone)
      postObj.phone = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('getLockCount', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 渠道 */
  // 供货授权关系查询
  customerAuthQuery (obj = {}) {
    return http('customerAuthQueryWens', { isStore: '1', ...obj }, 'POST').then(response => {
      if (response.data.data && response.data.data.list) {
        response.data.data = { list: formatList(response.data.data.list) }
      }
      if (response.data.data && response.data.data.list && !response.data.data.list.length) {
        response.data.status = 'noData'
      }
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
    // let cacheKey = 'customerAuthQueryWens_key'
    // let cacheVal = sessionStorage.getItem(cacheKey)
    // if (cacheVal) {
    //   return Promise.resolve(JSON.parse(cacheVal))
    // } else {
    //   return http('customerAuthQueryWens', { isStore: '1', ...obj }, 'POST').then(response => {
    //     if (response.data.data && response.data.data.list) {
    //       response.data.data = { list: formatList(response.data.data.list) }
    //     }
    //     if (response.data.data && response.data.data.list && !response.data.data.list.length) {
    //       response.data.status = 'noData'
    //     }
    //     if (response.data.success) {
    //       sessionStorage.setItem('customerAuthQueryWens_key', JSON.stringify(response.data))
    //     }
    //     return Promise.resolve(response.data)
    //   }).catch((error) => {
    //     return Promise.reject(error)
    //   })
    // }
  },
  // 关联申请（渠道）状态列表查询接口
  customerRelQuery (obj) {
    return http('customerRelQuery', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 关联申请（渠道）或取消关联（渠道）
  customerReq (obj) {
    return http('customerReq', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 用户可关联申请列表
  queryRelCustomerList (obj) {
    return http('queryRelCustomerList', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 渠道申请新增或修改接口
  customerReqSave (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedPwd = RSAUtils.encryptedString(key, obj.password)
      const encrypedPhone = RSAUtils.encryptedString(key, obj.phone)
      const encrypedIdcard = RSAUtils.encryptedString(key, obj.idcard)
      postObj.password = encrypedPwd
      postObj.phone = encrypedPhone
      postObj.idcard = encrypedIdcard
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('wsCustomerReqSave', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 开户——新增客户信息
  saveCustomerInfo (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      obj.phone && (postObj.phone = RSAUtils.encryptedString(key, obj.phone))
      obj.idCard && (postObj.idCard = RSAUtils.encryptedString(key, obj.idCard))
      obj.blNumber && (postObj.blNumber = RSAUtils.encryptedString(key, obj.blNumber))
      obj.businessNumber && (postObj.businessNumber = RSAUtils.encryptedString(key, obj.businessNumber))
      obj.taxNumber && (postObj.taxNumber = RSAUtils.encryptedString(key, obj.taxNumber))
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('saveCustomerInfo', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 开户——新增代扣协议
  saveWithholdInfo (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      obj.cardNumber && (postObj.cardNumber = RSAUtils.encryptedString(key, obj.cardNumber))
      // obj.name && (postObj.name = RSAUtils.encryptedString(key, obj.name))
      obj.phone && (postObj.phone = RSAUtils.encryptedString(key, obj.phone))
      obj.idCard && (postObj.idCard = RSAUtils.encryptedString(key, obj.idCard))
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('saveWithholdInfo', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 开户——发起代扣协议签署
  launchAccountSignFlow (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      // obj.name && (postObj.name = RSAUtils.encryptedString(key, obj.name))
      obj.phone && (postObj.phone = RSAUtils.encryptedString(key, obj.phone))
      obj.idCard && (postObj.idCard = RSAUtils.encryptedString(key, obj.idCard))
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('launchAccountSignFlow', postObj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——销售部申请——渠道申请列表
  // customerReqQuery (obj) {
  //   return http('customerReqQuery', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 审核进度查询
  customerReqProgressQuery () {
    return http('customerReqProgressQuery', {}, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询渠道是否申请买卖关系
  customerBuySellQuery (obj) {
    return http('customerBuySellQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 首页——根据地区查询渠道信息
  queryCustomer (obj, config) {
    return http('customerQuery', obj, 'POST', config).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询渠道信息
  customerInfo (obj) {
    return http('queryCustomerInfo4ItemDetail', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 首页——根据类型或者首页查询轮播图
  queryBanner (obj, config) {
    return http('adListQuery', { adplaceNumber: 'store', ...obj }, 'POST', config).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 渠道轮播图
  queryAd (obj) {
    return http('adListQuery', { adplaceNumber: 'shop', ...obj }, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 图片 */
  // 上传图片
  uploadImage (obj, fn) {
    photoCompress(obj, { 'Content-Type': 'multipart/form-data' }, fn)
  },
  // 获取图片绝对路径
  getImageFullUrl (obj) {
    let cacheVal = sessionStorage.getItem('allImgDomainName')
    if (cacheVal) {
      return Promise.resolve(cacheVal)
    } else {
      return http('getImageFullUrl', obj, 'POST').then(response => {
        return Promise.resolve(response.data)
      }).catch((error) => {
        return Promise.reject(error)
      })
    }
  },
  /** 商品/商品详情 */
  // 首页——查询全部商品分类数据
  queryGoodsCategory (obj, config) {
    let cacheKey = 'queryGoodsCategory_key'
    let cacheVal = sessionStorage.getItem(cacheKey)
    if (cacheVal) {
      return Promise.resolve(JSON.parse(cacheVal))
    } else {
      return http('itemClassQuery', { page: 1, pageSize: 9999, ...obj }, 'POST', config).then(response => {
        if (response.data.success) {
          sessionStorage.setItem(cacheKey, JSON.stringify(response.data))
        }
        return Promise.resolve(response.data)
      }).catch((error) => {
        return Promise.reject(error)
      })
    }
  },
  // 搜索商品
  // queryGoods (obj) {
  //   return http('queryGoods', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 批量价格查询
  itemPriceQuery (obj) {
    return http('itemPriceListQuery', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询商品价格和库存
  itemPriceStoreQuery (obj) {
    return http('itemPriceStoreQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 购物车批量下单判断
  itemIsBuy (obj) {
    return http('itemIsBuyAndVendibility', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 商品详情页——商品详情
  queryGoodsDetail (obj, config) {
    return http('queryGoodsDetail', obj, 'POST', config).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // goodsRestrictQuery (obj) {
  //   return http(DOMAIN_NAME + 'goods/goodsRestrictQuery.json', obj, 'POST').then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 商品列表查询接口
  // queryGoodsList (obj) {
  //   return http(DOMAIN_NAME + 'goods/queryGoodsList.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 获取批量商品重量范围 - WENS
  itemListWeightRangeQuery (obj) {
    return http('itemListWeightRangeQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取商品重量范围 - WENS
  itemWeightRangeQuery (obj) {
    return http('productWtRangeQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取批量商品最大最小订货量 - WENS
  // buyQuantityListQuery (obj) {
  //   return http(DOMAIN_NAME + 'goods/buyQuantityListQuery.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 获取批量商品库存 - WENS
  // itemListStoreQtyQuery (obj) {
  //   return http(DOMAIN_NAME + 'goods/itemListStoreQtyQuery.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  /** 评价 */
  // 这个接口后期会有商品详情合并
  // 交易管理——我的订单——商品综合评价
  queryCmpResult (obj) {
    return http('wsGetCmpResult', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 商品详情页——商品评论查询
  queryItemEvaluate (obj, config) {
    return http('wsItemEvaluateQuery', obj, 'POST', config).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询可选择的评论项
  queryItemEvaluateTag (obj) {
    return http('wsEvaluateTagQuery', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询订单评价项
  // orderCommentItemQuery (obj) {
  //   return http('orderEvaluateTagQuery', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 订单评价
  orderCommentAdd (obj) {
    return http('wsOrderEvaluateAdd', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 商品评价
  saveItemEvaluate (obj) {
    return http('wsItemEvaluateAdd', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 商品评论标签查询
  queryEvaluateLabel (obj) {
    return http('wsEvaluateLabelQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 购物车 */
  // 购物车列表
  queryCart (obj = {}) {
    return http('wsShoppingCartQuery', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 加入购物车
  saveGoods (obj) {
    return http('wsShoppingCartSave', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 购物车删除
  deleteCart (obj, config) {
    return http('wsShoppingCartDelete', obj, 'POST', config).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 购物车修改
  updateCart (obj, config) {
    return http('wsShoppingCartModify', obj, 'POST', config).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /**
   * 会员中心
   */
  /** 用户信息 */
  // 我的账户——账户信息——会员信息查询
  userInfo (obj = {}) {
    return http('getUserInfo', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——账户信息——会员信息修改
  updateUserInfo (obj) {
    return http('userInfoModify', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询用户类型
  customerGroupList (obj = {}) {
    return http('queryCustomerGroup', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 司机 */
  // 我的账户——司机信息维护——司机列表
  driverListQuery (obj) {
    return http('wsDriverListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——司机信息维护——车辆类型信息
  carTypeQuery (obj = {}) {
    return http('carTypeQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——司机信息维护——新增、修改司机
  driverAdd (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedDriverId = RSAUtils.encryptedString(key, obj.driverId)
      postObj.driverId = encrypedDriverId
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('driverAdd', postObj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——司机信息维护——司机信息新增校验
  driverCheck (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedDriverId = RSAUtils.encryptedString(key, obj.driverId)
      postObj.driverId = encrypedDriverId
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('driverCheck', postObj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——司机信息维护——司机删除
  driverDel (obj) {
    return http('driverDelete', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——收货地址——查询配送地址列表
  customerAddressQuery (obj) {
    return http('customerAddressQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——收货地址——配送地址新增
  customerAddressSave (obj) {
    return http('customerAddressSave', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——收货地址——配送地址修改
  customerAddressUpdate (obj) {
    return http('customerAddressUpdate', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——收货地址——配送地址删除
  customerAddressDelete (obj) {
    return http('customerAddressDelete', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 我的账户——收货地址——查询地区
  queryProvince (obj = {}) {
    let cacheKey = 'queryProvince_key'
    let cacheVal = sessionStorage.getItem(cacheKey)
    if (cacheVal) {
      return Promise.resolve(JSON.parse(cacheVal))
    } else {
      return http('admindivisionQuery', { level: '001', ...obj }, 'POST').then(response => {
        response.data.data = {
          data: response.data.data
        }

        if (response.data && response.data.data && response.data.data.data) {
          response.data.data.data.push({
            level: '000',
            levelName: '省',
            name: '其它',
            number: '000',
            parent: null,
            parentName: null
          })
        }
        if (response.data.success) {
          sessionStorage.setItem(cacheKey, JSON.stringify(response.data))
        }
        return Promise.resolve(response.data)
      }).catch((error) => {
        return Promise.reject(error)
      })
    }
  },
  // 获取省市区
  queryAddress (obj) {
    return http('admindivisionQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 通知公告/订货汇总 */
  // 通知公告详情
  notiListQuery (obj) {
    return http('notiListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 通知公告列表查询
  notiDetailQuery (obj) {
    return http('notiDetailQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 通知公告是否已读修改接口
  noticeIsReadUpdate (obj) {
    return http('noticeIsReadUpdate', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 通知公告未读数量查询
  noticeNoReadCountQuery (obj) {
    return http('noticeNoReadCountQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 历史搜索记录 */
  // // 查询搜索历史记录
  // recordSearch (obj = {}) {
  //   return http(DOMAIN_NAME + 'index/recordSearch.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // // 删除历史搜索记录
  // delRecordSearch (obj = {}) {
  //   return http(DOMAIN_NAME + 'index/delRecordSearch.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // // 删除历史搜索记录
  // addRecordSearch (obj = {}) {
  //   return http(DOMAIN_NAME + 'index/addRecordSearch.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  /** 退换货 */
  // // 交易管理——退货管理——退货申请列表
  // queryReturnOrderList (obj, config) {
  //   return http(DOMAIN_NAME + 'sale/queryReturnOrderList.json', obj, 'POST', config).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 退货申请单详情查询
  queryReturnOrderDetail (obj) {
    return http('returnOrderDetailQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 退货申请单新增
  saveReturnOrder (obj) {
    return http('returnOrderAdd', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 退货原因查询
  queryReason (obj) {
    return http('returnReasonListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 退货申请列表
  returnOrderListQuery (obj) {
    return http('returnOrderListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 退货申请删除
  returnOrderDelete (obj) {
    return http('returnOrderDelete', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 订单 */
  // 保存订单
  // purOrderSave (obj) {
  //   return http(DOMAIN_NAME + 'order/purOrderSave.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 保存订单 - WENS
  purOrderSave (obj) {
    return http('wsPurOrderSave', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 商品提交购买页——配送方式
  // queryDistribut (obj, config) {
  //   return http(DOMAIN_NAME + 'order/queryDistribut.json', obj, 'POST', config).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 订单列表
  orderListQuery (obj) {
    return http('wsOrderListQuery', obj).then(response => {
      if (response.data && response.data.data && response.data.data.list) {
        let list = []
        response.data.data.list.forEach(v => {
          if (v.orderStatus !== 'A') {
            list.push(v)
          }
        })
        response.data.data.list = list
      }
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 订单详情
  // queryOrderDetail (obj) {
  //   return http(DOMAIN_NAME + 'order/queryOrderDetail.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 订单状态数量接口
  orderQuantity (obj) {
    return http('wsOrderCentreQueryCOS', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 订单删除接口
  purOrderPurDelete (obj) {
    return http('wsPurOrderPurDelete', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 订单取消接口
  orderCancel (obj) {
    return http('wsOrderCancel', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 提货点 - WENS
  warehouseListQuery (obj) {
    return http('warehouseListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 要货日期和时间 - WENS
  needTimeQuery (obj) {
    return http('needTimeQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 订单汇总 - WENS
  orderSummary (obj) {
    return http('orderSummary', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 银行卡列表 - WENS
  bankListQuery (obj) {
    return http('bankListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch(error => {
      return Promise.reject(error)
    })
  },
  // 获取订单详情 - WENS
  queryOrderDetail (obj) {
    return http('queryOrderDetailWithOpTime', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 辅助资料查询接口
  assistantdataQuery (obj) {
    return http('assistantdataQueryWS', obj, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 查询EAS业务类型列表
  bizTypeListQuery (obj = {}) {
    let cacheKey = 'bizTypeListQuery_key'
    let cacheVal = sessionStorage.getItem(cacheKey)
    if (cacheVal) {
      return Promise.resolve(JSON.parse(cacheVal))
    } else {
      return http('searchBizTypeList', obj).then(response => {
        if (response.data.success) {
          sessionStorage.setItem(cacheKey, JSON.stringify(response.data))
        }
        return Promise.resolve(response.data)
      }).catch((error) => {
        return Promise.reject(error)
      })
    }
  },
  /** 关注中心模块 */
  // 关注店铺
  // 关注店铺的数量接口
  // shopConcernQty (obj) {
  //   return http(DOMAIN_NAME + 'concern/shopConcernQty.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 关注店铺接口
  // shopConcernQuery (obj) {
  //   return http(DOMAIN_NAME + 'concern/shopConcernQuery.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 关注店铺接口
  // shopConcernAdd (obj) {
  //   return http(DOMAIN_NAME + 'concern/shopConcernAdd.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 取消关注接口
  // shopConcernDel (obj) {
  //   return http(DOMAIN_NAME + 'concern/shopConcernDel.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 店铺关注状态
  // shopConcernState (obj) {
  //   return http(DOMAIN_NAME + 'concern/shopConcernState.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 收藏商品
  // 商品批量收藏
  // goodsCollectAddBatch (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectAddBatch.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 收藏商品查询接口
  // goodsCollectQuery (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectQuery.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 所有收藏商品id查询接口
  // goodsCollectQueryId (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectQueryId.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 收藏商品数量查询接口
  // goodsCollectQty (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectQty.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 当前登录用户进行收藏操作
  // goodsCollectAdd (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectAdd.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 当前登录用户进行取消收藏操作
  // goodsCollectDel (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectDel.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 单个商品收藏状态查询接口
  // goodsCollectState (obj) {
  //   return http(DOMAIN_NAME + 'collect/goodsCollectState.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  /** 资产中心模块 */
  // 收款记录查询列表
  receivingBillQueryList (obj) {
    return http('wsReceivingBillQueryList', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 收款记录详情
  // receivingBillQueryDetails (obj) {
  //   return http('receivingBillQueryDetails', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 预收款余额查询
  balanceQuery (obj) {
    return http('wsBalanceQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 积分 */
  // 会员积分查询（原）
  memberPointQuery (obj) {
    return http('pointsAccQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 积分明细列表（原）
  queryIntegralList (obj) {
    return http('pointsLogQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  /** 反馈意见 */
  // 反馈意见列表查询
  feedBackListQuery (obj) {
    return http('wsFeedBackListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 反馈意见详情查询
  feedBackDetailsQuery (obj) {
    return http('wsFeedBackDetailsQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 反馈意见添加
  feedBackAdd (obj) {
    return http('feedBackAdd', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 反馈意见关闭
  // feedBackClose (obj) {
  //   return http(DOMAIN_NAME + 'feedback/feedBackClose.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  /** 受托人 */
  // 获取受托人列表 - WENS
  queryDelegation (obj) {
    return http('delegationUserListQuery', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取受托人详情 - WENS
  // delegationDetail (obj) {
  //   return http('delegationDetail', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 添加受托人 - WENS
  addDelegation (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      const encrypedIdcard = RSAUtils.encryptedString(key, obj.idcard)
      const encrypedPhone = RSAUtils.encryptedString(key, obj.telephone)
      postObj.idcard = encrypedIdcard
      postObj.telephone = encrypedPhone
      postObj.keyCode = keyPair.publicKeyCode
    }
    return http('delegationUserAdd', postObj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 修改受托人 - WENS
  updateDelegation (obj) {
    let postObj = { ...obj }
    let wsmallPublickeyData = sessionStorage.getItem('wsmall_publickey_data')
    if (wsmallPublickeyData) {
      let keyPair = JSON.parse(wsmallPublickeyData)
      RSAUtils.setMaxDigits(200)
      const key = RSAUtils.getKeyPair(
        keyPair.publicKeyExponent,
        '',
        keyPair.publicKeyModulus
      )
      if (obj.phone) {
        const encrypedPhone = RSAUtils.encryptedString(key, obj.phone)
        postObj.phone = encrypedPhone
        postObj.keyCode = keyPair.publicKeyCode
      }
    }
    return http('updateDelegation', postObj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 删除受托人 - WENS
  // deleteDelegation (obj) {
  //   return http('deleteDelegation', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  /** 发货单 */
  // 获取发货单详情 - WENS
  queryBroilerSellOrderDetail (obj) {
    return http('callV10', { url: 'nextPlatformLink/trade/broilerInvoice/detail', ...obj }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取批量发货单详情
  queryBroilerSellOrderListDetail (obj) {
    return http('callV10', { url: 'nextPlatformLink/trade/broilerInvoice/list/detail', ...obj }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取WX接口注入权限验证配置 - WENS
  wxInfo (obj) {
    return http('wxInfo', obj).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 扫描绑定QRCode与发货单fid - WENS
  bindDeliveryFidToQRCode (obj) {
    return http('callV10', { url: 'nextPlatformLink/trade/deliveryFid_To_QRCode', ...obj }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 发货单付款 - WENS
  // payBroilerOrder (obj) {
  //   return http(DOMAIN_NAME + 'billWens/payBroilerOrder.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 发货单批量付款 - WENS
  // payBroilerOrderList (obj) {
  //   return http(DOMAIN_NAME + 'billWens/payBroilerOrderList.json', obj).then(response => {
  //     return Promise.resolve(response.data)
  //   }).catch((error) => {
  //     return Promise.reject(error)
  //   })
  // },
  // 发货单批量查询打印信息 - WENS
  queryBrlrSellOrderListPrtInfo (obj) {
    return http('callV10', { url: 'nextPlatformLink/trade/broilerInvoice/list/printInfo', ...obj }).then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // 获取服务器时间
  getServerTime () {
    return http('getTime', {}, 'POST').then(response => {
      let bjT = (response && response.headers && response.headers.date && new Date(response.headers.date)) || new Date() // 北京时间
      return Promise.resolve(bjT)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // api通用接口 - WENS
  commonCall (obj = {}, method) {
    // 为了减少请求，保存至sessionStorage
    let cacheList = [ // apiCode列表
      // 'userRelCusQuery'
      'queryAdPlace'
    ]
    if (cacheList.includes(method)) {
      let cacheVal = sessionStorage.getItem(method + '_key')
      if (cacheVal) {
        return Promise.resolve(JSON.parse(cacheVal))
      } else {
        return http(method, obj, 'POST').then(response => {
          if (response.data.success) {
            sessionStorage.setItem(method + '_key', JSON.stringify(response.data))
          }
          return Promise.resolve(response.data)
        }).catch((error) => {
          return Promise.reject(error)
        })
      }
    }

    return http(method, obj, 'POST').then(response => {
      let bjT = (response && response.headers && response.headers.date && new Date(response.headers.date)) || new Date() // 北京时间
      response.data.serverTime = bjT
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  // api通用接口v10 - WENS
  commonCall_v10 (obj, url) {
    return http('callV10', { url, ...obj }, 'POST').then(response => {
      return Promise.resolve(response.data)
    }).catch((error) => {
      return Promise.reject(error)
    })
  }
}

/**
 * axios 函数封装
 * @param mtd 参数method
 * @param obj 参数data
 * @param method 请求方式
 * @param config headers 配置
 * @returns {AxiosPromise}
 */
export function http (mtd, obj = {}, method = 'POST', config = {}) {
  return axios({
    method: method,
    url: DOMAIN_NAME + 'kapi/app/mdr/mdrApi/invoke?method=' + mtd,
    data: {
      comeFrom: 'mobile', // 给所有请求添加统一传参comeFrom
      method: mtd,
      data: obj
    },
    headers: {
      ...config
    }
  })
}

/**
 * axios 函数封装
 * @param url 接口地址
 * @param params 参数
 * @param method 请求方式
 * @param config headers 配置
 * @returns {AxiosPromise}
 */
export function httpTwo (url, params = {}, method = 'POST', config = {}) {
  return axios({
    method: method,
    url: url,
    data: params,
    headers: config
  })
}

/**
 * axios get函数封装
 * @param url 接口地址
 * @param params 参数
 * @param method 请求方式
 * @returns {AxiosPromise}
 */
export function httpGet (url, params = {}) {
  return axios({
    method: 'GET',
    url: url,
    params: params
  })
}

export default api
