import { DOMAIN_NAME, ERR_OK, IK_ARRAY } from './config'
import api from './api'

export { api, DOMAIN_NAME, ERR_OK, IK_ARRAY }
