module.exports = ({ file }) => {
  let rootValue = 75

  /** vant使用的rootValue为37.5 */
  if (file && file.dirname && file.dirname.indexOf('vant') > -1) {
    rootValue = 37.5
  } else {
    rootValue = 75
  }

  return {
    plugins: {
      autoprefixer: {
        overrideBrowserslist: ['Android >= 4.0', 'iOS >= 7']
      },
      'postcss-pxtorem': {
        rootValue: rootValue,
        propList: ['*'],
        minPixelValue: 2,
        selectorBlackList: ['video-js', 'pdfjs']
      }
    }
  }
}
