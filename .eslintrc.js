module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'prefer-const': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "generator-star-spacing": "off",
    "no-tabs":"off",
    "no-unused-vars":"off",
    "no-irregular-whitespace":"off"
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  globals: {
    RSAUtils: 'writable',
    AMap: 'writable',
    AMapUI: 'writable',
    ud: 'writable'
  }
}
